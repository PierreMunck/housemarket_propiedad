<?php

// Define path to application directory
defined('APPLICATION_PATH') || define('PUBLIC_HTML_PATH', realpath(dirname(__FILE__)));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../housemarketpropiedad/application'));
/* Si no esta definida esta variable de entorno en los archivos de configuracion del servidor web, no se cargara la configuracion de desarrollo
 * y se cargará por defecto la configuracion de produccion, generando un redireccionamiento a hmapp.com
*/
// Define application environment.
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH . '/../library/'),
		realpath(APPLICATION_PATH . '/models/'),
		realpath(APPLICATION_PATH . '/views/'),
		realpath(APPLICATION_PATH . '/controllers/'),
		get_include_path(),
)));

/** Zend_Application * */
require_once 'Zend/Application.php';
require_once 'Zend/Config/Ini.php';

$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . '/configs/application.ini'
);

// application bootstrap
try{
	$application->bootstrap();
}catch(Exception $e){

}

$application->run();
?>