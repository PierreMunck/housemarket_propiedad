# Update from the 1.8.7 version
# Add the table prefix if needed
# ---------------------------------

## first backup your current phpdig tables
## capitalizations represent your database information
## http://dev.mysql.com/doc/mysql/en/mysqldump.html
# shell> mysqldump -h LOCALHOST -u DBUSER -p --opt DBNAME clicks engine excludes includes keywords logs site_page sites spider tempspider > phpdig187backup.sql

## http://dev.mysql.com/doc/mysql/en/myisamchk-syntax.html
# if you change the character set when running mysql, that may also change the sort order, 
# so you may need to run myisamchk -r -q --set-character-set=utf8 on the tables, or your 
# indexes might not be ordered correctly. also, utf-8 can take more space than other 
# encodings, so you may need to edit my.cnf and increase the max_allowed_packet size, and 
# then restart mysql.

## find your mysql character set (iso-8859-1 aka latin1 is default)
# SHOW VARIABLES LIKE '%character%';

## run the following alter queries
## http://dev.mysql.com/doc/mysql/en/create-table.html
## http://dev.mysql.com/doc/mysql/en/alter-table.html
ALTER TABLE clicks ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE engine ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE excludes ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE includes ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE keywords ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE logs ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE site_page ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE sites ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE spider ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE tempspider ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

## make another backup of your phpdig tables for use in conversion to utf-8
## capitalizations represent your database information
## http://dev.mysql.com/doc/mysql/en/mysqldump.html
# shell> mysqldump -h LOCALHOST -u DBUSER -p --opt DBNAME clicks engine excludes includes keywords logs site_page sites spider tempspider > phpdigdump.sql

## convert the phpdig tables to utf-8
## change iso-8859-1 to your mysql character set
# shell> iconv -f iso-8859-1 -t utf8 phpdigdump.sql > phpdigutf8.sql

## overwrite the old tables with the new converted tables
## capitalizations represent your database information
## http://dev.mysql.com/doc/mysql/en/mysql.html
# shell> mysql -h LOCALHOST -u DBUSER -p DBNAME < phpdigutf8.sql

## some extra mysql stuff
# shows status of tables
# SHOW TABLE STATUS;
# shows available character sets
# SHOW CHARACTER SET;
# shows utf collations
# SHOW COLLATION LIKE 'utf%';
# show collations and charsets
# SHOW COLLATION;
