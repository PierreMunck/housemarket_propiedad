See the /locales/ascii/XX-language.php files for contributors.

Thanks to these people for the translations.

Note that some XX-language files were created prior to the current version of 
PhpDig so you may have to edit your language file.

Use \' for ' in the file.

For example: 

	'example'   =>'Here\'s an example.',

No comma after LAST array element.

UPGRADING FROM PhpDig v.1.8.7 to PhpDig 1.8.8 RC1

You may need a UTF-8 acceptable editor to edit the /locales/XX-language.php files.

You can use a TEXT editor to edit the /locales/ascii/XX-language.php files.

If you edit the files in the ascii directory, you will need to convert to UTF-8.

For example:

	shell> iconv -f windows-1251 -t utf-8 -o ru-language.phpx ru-language.php &
	shell> iconv -f iso-8859-2 -t utf-8 -o cs-language.phpx cs-language.php &

The UTF-8 encoded files would then end with phpx so you need to move/rename them.

In general:

	shell> iconv -f <from_encoding> -t <to_encoding> [-o <output_file>] <input_file>

The /locales/XX-language.php files are already in UTF-8 format.

The /locales/ascii/XX-language.php files are in ASCII format.

If a UTF-8 file is in incorrect format, simply run iconv on the ASCII version.

FTP the /locales/XX-language.php files in BINARY format.

FTP the /locales/ascii/XX-language.php files in ASCII format.
