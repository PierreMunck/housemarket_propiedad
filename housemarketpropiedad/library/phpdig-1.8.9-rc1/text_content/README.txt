UPGRADING FROM PhpDig v.1.8.7 to PhpDig 1.8.8 RC1

You may need a UTF-8 acceptable editor to convert the /text_content/XXX.txt files.

Otherwise, you can use iconv to convert the /text_content/XXX.txt files to UTF-8.

For example:

	shell> iconv -f windows-1251 -t utf-8 -o XXX.txtx XXX.txt &
	shell> iconv -f iso-8859-2 -t utf-8 -o XXX.txtx XXX.txt &

The UTF-8 encoded files would then end with txtx so you need to move/rename them.

In general:

	shell> iconv -f <from_encoding> -t <to_encoding> [-o <output_file>] <input_file>

The /text_content/XXX.txt files are not in UTF-8 format so you need to convert them.

