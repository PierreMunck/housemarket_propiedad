This folder contains PhpDig v.1.8.7 documentation.

PhpDig v.1.8.8+ RC1 has some different options and 
functionality than PhpDig v.1.8.7.

PhpDig v.1.8.8+ RC1 is an experimental release.

Some of the PhpDig v.1.8.7 documentation may help 
you. Some of the PhpDig v.1.8.7 documentation is 
no longer applicable.

Documentation will be updated if/as time permits.

Visit: http://www.phpdig.net

