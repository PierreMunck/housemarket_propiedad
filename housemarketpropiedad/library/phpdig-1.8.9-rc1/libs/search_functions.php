<?php
/*
----------------------------------------------------------------------------------
PhpDig Version 1.8.x - See the config file for the full version number.
This program is provided WITHOUT warranty under the GNU/GPL license.
See the LICENSE file for more information about the GNU/GPL license.
Contributors are listed in the CREDITS and CHANGELOG files in this package.
Developer from inception to and including PhpDig v.1.6.2: Antoine Bajolet
Developer from PhpDig v.1.6.3 to and including current version: Charter
Copyright (C) 2001 - 2003, Antoine Bajolet, http://www.toiletoine.net/
Copyright (C) 2003 - current, Charter, http://www.phpdig.net/
Contributors hold Copyright (C) to their code submissions.
Do NOT edit or remove this copyright or licence information upon redistribution.
If you modify code and redistribute, you may ADD your copyright to this notice.
----------------------------------------------------------------------------------
*/

//===============================================
// do the search and display the results
function phpdigSearch($id_connect, $query_string, $option='start', $refine=0, $refine_url='', $lim_start=0, $limite=10, 
   $browse=0, $site=0, $path='', $relative_script_path = '.', $template='', $adlog_flag=0, $rssdf='', $template_demo='') {

//---------- start check input to phpdigSearch function

// $id_connect set in connect.php file
// $query_string cleaned in $query_to_parse in search_functions.php file
if (($option != "start") && ($option != "any") && ($option != "exact")) {
    $option = SEARCH_DEFAULT_MODE;
}
if (($refine != 0) && ($refine != 1)) {
    $refine = 0;
}
// $refine_url set in search_functions.php file
settype($lim_start,'integer');
settype($limite,'integer');
if (($limite != 10) && ($limite != 30) && ($limite != 100)) {
    $limite = SEARCH_DEFAULT_LIMIT;
}
if (($browse != 0) && ($browse != 1)) {
    $browse = 0;
}
if (mb_eregi("^[0-9]+[,]",$site)) {
    $tempbust = explode(",",$site);
    $site = $tempbust[0];
    $path = $tempbust[1];
}
settype($site,'integer'); // now set to integer
settype($path,'string');  // make sure set to string
if (!get_magic_quotes_gpc()) {
    $my_path = addslashes($path);
}
else {
    $my_path = addslashes(mb_ereg_replace("\\\\","",stripslashes($path)));
    $path = stripslashes($path);
}
if (empty($site) && empty($path)) {
    $refine = 0;
}
else {
    $refine = 1;
}
if ($path == "-###-") {
    $site = 0;
    $path = "";
    $refine = 0;
}
// $relative_script_path set in search.php file
// $template set in config.php file
// $adlog_flag set in search.php file
// $rssdf set in search.php file
// $template_demo set in config.php

//---------- end check input to phpdigSearch function

$timer = new phpdigTimer('html');
$timer->start('All');

// init variables
settype($maxweight,'integer');
$ignore = '';
$ignore_common = '';
$ignore_message = '';
$ignore_commess = '';
$wheresite = '';
$wherepath = '';
$table_results = '';
$final_result = '';
$search_time = 0;
$strings = '';
$num_tot = 0;
$leven_final = "";
$exclude = array();
$nav_bar = '';
$pages_bar = '';
$previous_link = '';
$next_link = '';
$stop_regs1 = "";
$stop_regs2 = "";

$mtime = explode(' ',microtime());
$start_time = $mtime[0]+$mtime[1];

$timer->start('All backend');
$timer->start('parsing strings');

if ($query_string) {
    if (ENABLE_JPKANA == true) {
      $query_string = @mb_convert_kana($query_string,CONVERT_JPKANA,"UTF-8");
    }
    $query_string = phpdigVerifyUTF8($query_string);
}

// the query was filled
if ($query_string) {

$common_words = phpdigComWords("$relative_script_path/includes/common_words.txt");

$like_start = array( "start" => "%",
                     "any" => "%",
                     "exact" => "%"
                     );
$like_end = array( "start" => "%",
                     "any" => "%",
                     "exact" => "%"
                     );
$like_operator = array( "start" => "like",
                     "any" => "like",
                     "exact" => "like"
                     );

if ($refine) {
     $wheresite = "AND spider.site_id = $site ";
     if (($path) && (mb_strlen($path) > 0)) {
          $my_path = str_replace(" ","\%20",$my_path);
          $wherepath = "AND spider.path = '$my_path' ";
     }
     $refine_url = "&amp;refine=1&amp;site=$site&amp;path=".urlencode($path);
}
else {
     $refine_url = "";
}

if ($lim_start < 0) {
     $lim_start = 0;
}

$n_words = count(explode(" ",$query_string));

$ncrit = 0;
$tin = "0";

if (!get_magic_quotes_gpc()) {
    $query_to_parse = addslashes($query_string);
}
else {
    $query_to_parse = $query_string;
}
$my_query_string_link = stripslashes($query_to_parse);

$query_to_parse = mb_ereg_replace('_','\_',$query_to_parse); // avoid _ in the query
$query_to_parse = mb_ereg_replace('%','\%',$query_to_parse); // avoid % in the query
$query_to_parse = mb_ereg_replace('(\x00|\\\x00)','',$query_to_parse); // avoid null byte in query
$query_to_parse = mb_ereg_replace('(\x1a|\\\x1a)','',$query_to_parse); // avoid sub byte in query
$query_to_parse = mb_ereg_replace('\]','',$query_to_parse); // avoid ] in the query
$query_to_parse = mb_ereg_replace('\[','',$query_to_parse); // avoid [ in the query
$query_to_parse = mb_ereg_replace('>','',$query_to_parse); // avoid > in the query
$query_to_parse = mb_ereg_replace('<','',$query_to_parse); // avoid < in the query
$query_to_parse = mb_ereg_replace('\}','',$query_to_parse); // avoid } in the query
$query_to_parse = mb_ereg_replace('\{','',$query_to_parse); // avoid { in the query
$query_to_parse = mb_ereg_replace('\)','',$query_to_parse); // avoid ) in the query
$query_to_parse = mb_ereg_replace('\(','',$query_to_parse); // avoid ( in the query
$query_to_parse = mb_ereg_replace('\$','',$query_to_parse); // avoid $ in the query
$query_to_parse = mb_ereg_replace('&[^ ]+','',$query_to_parse); // avoid &[chars] in the query
$query_to_parse = mb_ereg_replace('[?]*','',$query_to_parse); // avoid ? in the query

$query_to_parse = mb_ereg_replace("^([\x00-\x1f]|[\x21-\x2c]|[\x2e-\x2f]|[\x3a-\x40]|[\x5b-\x60]|[\x7b-\x7f])+","",$query_to_parse); //off front only
$query_to_parse = mb_ereg_replace("([\x00-\x1f]|[\x21-\x2c]|[\x2e-\x2f]|[\x3a-\x40]|[\x5b-\x60]|[\x7b-\x7f])+$","",$query_to_parse); //off back only
$query_to_parse = trim(mb_ereg_replace(" ([\x00-\x1f]|[\x21-\x2c]|[\x2e-\x2f]|[\x3a-\x40]|[\x5b-\x60]|[\x7b-\x7f])+"," ",$query_to_parse));
$query_to_parse = trim(mb_ereg_replace("([\x00-\x1f]|[\x21-\x2c]|[\x2e-\x2f]|[\x3a-\x40]|[\x5b-\x60]|[\x7b-\x7f])+ "," ",$query_to_parse));

$query_to_parse = mb_strtolower($query_to_parse); //made all lowercase
$query_to_parse = trim(mb_ereg_replace(" +"," ",$query_to_parse)); // no more than one space

$query_for_strings = $query_to_parse;
$query_for_phrase = $query_to_parse;
$test_short = $query_to_parse;
$query_to_parse2 = explode(" ",$query_to_parse);
usort($query_to_parse2, "phpdigByLength");
$query_to_parse = implode(" ",$query_to_parse2);
if (isset($query_to_parse2)) { unset($query_to_parse2); }

if (SMALL_WORDS_SIZE >= 1) {
$ignore_short_flag = 0;
$test_short_counter = 0;
$test_short2 = explode(" ",$test_short);
for ($i=0; $i<count($test_short2); $i++) {
    $test_short2[$i] = trim($test_short2[$i]);
}
$test_short2 = array_unique($test_short2);
sort($test_short2);
$test_short3 = array();
for ($i=0; $i<count($test_short2); $i++) {
  if ((mb_strlen($test_short2[$i]) <= SMALL_WORDS_SIZE) && (mb_strlen($test_short2[$i]) > 0)) {
    $test_short2[$i].=" ";
    $test_short_counter++;
    $test_short3[] = $test_short2[$i];
  }
}
$test_short = implode(" ",$test_short3);
if (isset($test_short2)) { unset($test_short2); }
if (isset($test_short3)) { unset($test_short3); }

  while (mb_ereg('( [^ ]{1,'.SMALL_WORDS_SIZE.'} )|( [^ ]{1,'.SMALL_WORDS_SIZE.'})$|^([^ ]{1,'.SMALL_WORDS_SIZE.'} )',$test_short,$regs)) {
     for ($n=1; $n<=3; $n++) {
        if (($regs[$n]) || ($regs[$n] == 0)) {
            $ignore_short_flag++;
            if (!mb_eregi("\"".trim(stripslashes($regs[$n]))."\", ",$ignore)) {
              $ignore .= "\"".trim(stripslashes($regs[$n]))."\", ";
            }
            $test_short = trim(str_replace($regs[$n],"",$test_short));
        }
     }
  }
  if (mb_strlen($test_short) <= SMALL_WORDS_SIZE) {
    if (!mb_eregi("\"".$test_short."\", ",$ignore)) {
      $ignore_short_flag++;
      $ignore .= "\"".stripslashes($test_short)."\", ";
    }
    $test_short = trim(str_replace($test_short,"",$test_short));
  }
}

$ignore = mb_ereg_replace("\"\", ","",$ignore);

if ($option != "exact") {
  if (($ignore) && ($ignore_short_flag > 1) && ($test_short_counter > 1)) {
    $ignore_message = $ignore.' '.phpdigMsg('w_short_plur');
  }
  elseif ($ignore) {
    $ignore_message = $ignore.' '.phpdigMsg('w_short_sing');
  }
}

$ignore_common_flag = 0;
while (mb_ereg("(-)?([^ ]{".(SMALL_WORDS_SIZE+1).",}).*",$query_for_strings,$regs)) {
        $query_for_strings = trim(str_replace($regs[2],"",$query_for_strings));
        if (!isset($common_words[stripslashes($regs[2])])) {
             if ($regs[1] == '-') {
                 $exclude[$ncrit] = $regs[2];
                 $query_for_phrase = trim(str_replace("-".$regs[2],"",$query_for_phrase));
             }
             else {
                 $strings[$ncrit] = $regs[2];
             }
             $kconds[$ncrit] = '';

             $kconds[$ncrit] .= " AND k.keyword ".$like_operator[$option]." '".$like_start[$option].$regs[2].$like_end[$option]."' ";
             $ncrit++;
        }
        else {
             $ignore_common_flag++;
             $ignore_common .= "\"".stripslashes($regs[2])."\", ";
        }
}

if ($option != "exact") {
  if (($ignore_common) && ($ignore_common_flag > 1)) {
    $ignore_commess = $ignore_common.' '.phpdigMsg('w_common_plur');
  }
  elseif ($ignore_common) {
    $ignore_commess = $ignore_common.' '.phpdigMsg('w_common_sing');
  }
}

$timer->stop('parsing strings');

if ($ncrit && is_array($strings)) {
     $query = "SET OPTION SQL_BIG_SELECTS = 1";
     mysql_query($query,$id_connect);

     $my_spider2site_array = array();
     $my_sitecount_array = array();

     for ($n = 0; $n < $ncrit; $n++) {
           $timer->start('spider queries');

           $query = "SELECT spider.spider_id,sum(weight) as weight, spider.site_id
           FROM ".PHPDIG_DB_PREFIX."keywords as k,".PHPDIG_DB_PREFIX."engine as engine, ".PHPDIG_DB_PREFIX."spider as spider
           WHERE engine.key_id = k.key_id
           ".$kconds[$n]."
           AND engine.spider_id = spider.spider_id $wheresite $wherepath
           GROUP BY spider.spider_id,spider.site_id ";
           $result = mysql_query($query,$id_connect);
           $num_res_temp = mysql_num_rows($result);

           $timer->stop('spider queries');
           $timer->start('spider fills');

           if ($num_res_temp > 0) {
               if (!isset($exclude[$n])) {
               $num_res[$n] = $num_res_temp;
                    while (list($spider_id,$weight,$site_id) = mysql_fetch_array($result)) {
                         $s_weight[$n][$spider_id] = $weight;
                         $my_spider2site_array[$spider_id] = $site_id;
                         $my_sitecount_array[$site_id] = 0;
                    }
               }
               else {
               $num_exclude[$n] = $num_res_temp;
                     while (list($spider_id,$weight) = mysql_fetch_array($result)) {
                            $s_exclude[$n][$spider_id] = 1;
                     }
               mysql_free_result($result);
               }
           }
           elseif (!isset($exclude[$n])) {
                   $num_res[$n] = 0;
                   $s_weight[$n][0] = 0;
          }
          $timer->stop('spider fills');
     }

     $timer->start('reorder results');

     if ($option != "any") {
     if (is_array($num_res)) {
           asort ($num_res);
           list($id_most) = each($num_res);
           reset ($s_weight[$id_most]);
           while (list($spider_id,$weight) = each($s_weight[$id_most]))  {
                  $weight_tot = 1;
                  reset ($num_res);
                  while(list($n) = each($num_res)) {
                        settype($s_weight[$n][$spider_id],'integer');
                        $weight_tot *= sqrt($s_weight[$n][$spider_id]);
                  }
                  if ($weight_tot > 0) {
                       $final_result[$spider_id]=$weight_tot;
                  }
           }
     }
     }
     else {
     if (is_array($num_res)) {
           asort ($num_res);
           while (list($spider_id,$site_id) = each($my_spider2site_array))  {
                  $weight_tot = 0;
                  reset ($num_res);
                  while(list($n) = each($num_res)) {
                        settype($s_weight[$n][$spider_id],'integer');
                        $weight_tot += sqrt($s_weight[$n][$spider_id]);
                  }
                  if ($weight_tot > 0) {
                       $final_result[$spider_id]=$weight_tot;
                  }
           }
     }
     }

    if (isset($num_exclude) && is_array($num_exclude)) {
           while (list($id) = each($num_exclude)) {
                  while(list($spider_id) = each($s_exclude[$id])) {
                        if (isset($final_result[$spider_id])) { unset($final_result[$spider_id]); }
                  }
           }
    }

    if ($option == "exact") {
      if ((is_array($final_result)) && (count($final_result) > 0)) {
        $exact_phrase_flag = 0;
        arsort($final_result);
        reset($final_result);
        $query_for_phrase_array = explode(" ",$query_for_phrase);
        $reg_strings = mb_ereg_replace('@#@',' ',mb_ereg_replace('\\\\','',implode('@#@',$query_for_phrase_array)));
        $reg_strings = "($stop_regs1)($reg_strings)($stop_regs2)";
        $reg_strings = phpdigRegStrings($reg_strings);
        while (list($spider_id,$weight) = each($final_result)) {
          $content_file = $relative_script_path.'/'.TEXT_CONTENT_PATH.$spider_id.'.txt';
          if (is_file($content_file) && CONTENT_TEXT == 1) {
            $f_handler = fopen($content_file,'rb');
            $extract_content = mb_ereg_replace("([ ]{2,}|\n|\r|\r\n|\t)"," ",fread($f_handler,filesize($content_file)));
              if (!mb_eregi($reg_strings,$extract_content)) {
                $exact_phrase_flag = 1;
              }
            fclose($f_handler);
          }
          elseif (CONTENT_TEXT == 0) {
            $exact_query = "SELECT first_words FROM ".PHPDIG_DB_PREFIX."spider WHERE spider_id=$spider_id";
            $exact_result = mysql_query($exact_query,$id_connect);
            list($exact_content) = mysql_fetch_row($exact_result);
            if (!mb_eregi($reg_strings,$exact_content)) {
              $exact_phrase_flag = 1;
            }
          }
          if ($exact_phrase_flag == 1) {
            if (isset($final_result[$spider_id])) { unset($final_result[$spider_id]); }
            $exact_phrase_flag = 0;
          }
        }
      }
    }

    if((!$refine) && (NUMBER_OF_RESULTS_PER_SITE != -1)) {
       if ((is_array($final_result)) && (count($final_result) > 0)) {
           arsort($final_result);
           reset($final_result);
           while (list($spider_id,$weight) = each($final_result)) {
                  $site_id = $my_spider2site_array[$spider_id];
                  $current_site_counter = $my_sitecount_array[$site_id];
                  if ($current_site_counter < NUMBER_OF_RESULTS_PER_SITE) {
                         $my_sitecount_array[$site_id]++;
                  }
                  else {
                         if (isset($final_result[$spider_id])) { unset($final_result[$spider_id]); }
                  }
           }
       }
    }

    $timer->stop('reorder results');
}

$timer->stop('All backend');
$timer->start('All display');

if ((is_array($final_result)) && (count($final_result) > 0)) {
    arsort($final_result);
    $lim_start = max(0, $lim_start-($lim_start % $limite));
    $n_start = $lim_start+1;
    $num_tot = count($final_result);

    if ($n_start+$limite-1 < $num_tot) {
           $n_end = ($lim_start+$limite);
           $more_results = 1;
    }
    else {
          $n_end = $num_tot;
          $more_results = 0;
    }

    if ($n_start > $n_end) {
        $n_start = 1;
        $n_end = min($num_tot,$limite);
        $lim_start = 0;
        if ($n_end < $num_tot) {
            $more_results = 1;
        }
    }

    // mb_ereg for text snippets and highlighting
    if ($option == "exact") {
        $reg_strings = mb_ereg_replace('@#@',' ',mb_ereg_replace('\\\\','',implode('@#@',$query_for_phrase_array)));
    }
    else {
        $reg_strings = mb_ereg_replace('@#@','|',mb_ereg_replace('\\\\','',implode('@#@',$strings)));
    }

    switch($option) {
        case 'any':
        $reg_strings = "($stop_regs1)($reg_strings)()";
        $reg_strings = phpdigRegStrings($reg_strings);
        break;
        case 'exact':
        $reg_strings = "($stop_regs1)($reg_strings)($stop_regs2)";
        $reg_strings = phpdigRegStrings($reg_strings);
        break;
        default:
        $reg_strings = "($stop_regs1)($reg_strings)()";
        $reg_strings = phpdigRegStrings($reg_strings);
    }

    $timer->start('Result table');

    //fill the results table
    reset($final_result);
    for ($n = 1; $n <= $n_end; $n++) {
        list($spider_id,$s_weight) = each($final_result);
        if (!$maxweight) {
              $maxweight = $s_weight;
        }
        if ($n >= $n_start) {
             $timer->start('Display queries');

             $query = "SELECT sites.site_url, sites.port, spider.path,spider.file,spider.first_words,sites.site_id,spider.spider_id,spider.last_modified,spider.md5 "
                      ."FROM ".PHPDIG_DB_PREFIX."spider AS spider, ".PHPDIG_DB_PREFIX."sites AS sites "
                      ."WHERE spider.spider_id=$spider_id AND sites.site_id = spider.site_id";
             $result = mysql_query($query,$id_connect);
             $content = mysql_fetch_array($result,MYSQL_ASSOC);
             mysql_free_result($result);
             if ($content['port']) {
                 $content['site_url'] = mb_ereg_replace('/$',':'.$content['port'].'/',$content['site_url']);
             }
             $weight = sprintf ("%01.2f", (100*$s_weight)/$maxweight);
             $url = mb_eregi_replace("(.*)[^:][/]{2,}","\\1/",urldecode($content['site_url'].$content['path'].$content['file']));

             $js_url = urlencode(mb_eregi_replace("^[a-z]{3,5}://","",$url));

             $url = mb_ereg_replace("\"","%22",mb_ereg_replace("'","%27",mb_ereg_replace(" ","%20",trim($url))));

             $l_site = "<a class='phpdig' href='".SEARCH_PAGE."?refine=1&amp;template_demo=".$template_demo."&amp;query_string=".urlencode($my_query_string_link)."&amp;site=".$content['site_id']."&amp;limite=$limite&amp;option=$option'>".htmlspecialchars(urldecode($content['site_url']),ENT_QUOTES)."</a>";
             if ($content['path']) {
                  $content['path'] = urlencode(urldecode($content['path']));
                  $content2['path'] = htmlspecialchars(urldecode($content['path']),ENT_QUOTES);
                  $l_path = ", ".phpdigMsg('this_path')." : <a class='phpdig' href='".SEARCH_PAGE."?refine=1&amp;template_demo=".$template_demo."&amp;query_string=".urlencode($my_query_string_link)."&amp;site=".$content['site_id']."&amp;path=".$content['path']."&amp;limite=$limite&amp;option=$option' >".$content2['path']."</a>";
             }
             else {
                  $content2['path'] = "";
                  $l_path="";
             }

             $first_words = $content['first_words'];
             list($title,$text) = explode("\n",$first_words);

             $timer->stop('Display queries');
             $timer->start('Extracts');

             $extract = "";
             $num_extracts = 0;
             $extract_size = SNIPPET_DISPLAY_LENGTH;

             if (CONTENT_TEXT == 1 && DISPLAY_SNIPPETS) {
                $content_file = $relative_script_path.'/'.TEXT_CONTENT_PATH.$content['spider_id'].'.txt';
                if (is_file($content_file)) {
                    $size_for_while = filesize($content_file);
                    while (($num_extracts == 0) && ($extract_size <= $size_for_while)) {
                       $f_handler = fopen($content_file,'rb');
                       while($num_extracts < DISPLAY_SNIPPETS_NUM && $extract_content = mb_ereg_replace("([ ]{2,}|\n|\r|\r\n|\t)"," ",fread($f_handler,$extract_size))) {
                           $extractinfo = phpdigGetExtracts($num_extracts,$reg_strings,$extract_content,$query_string);
                           $extract .= $extractinfo[0];
                           $num_extracts = $extractinfo[1];
                       }
                       fclose($f_handler);
                       $extract_size = phpdigExtractSize($extract_size,$size_for_while);
                    }
                }
             }
             elseif (CONTENT_TEXT == 0 && DISPLAY_SNIPPETS) {
                $extract_content = mb_ereg_replace("([ ]{2,}|\n|\r|\r\n|\t)"," ",$text);
                if (mb_strlen(trim($extract_content)) > 0) {
                    $where_to_start = 0;
                    $get_snippet_length = 1;
                    $size_for_while = mb_strlen($extract_content);
                    while (($num_extracts == 0) && ($extract_size <= $size_for_while)) {
                       while($num_extracts < DISPLAY_SNIPPETS_NUM && $get_snippet_length > 0) {
                           $snippet_content = mb_substr($extract_content,$where_to_start,$extract_size);
                           $get_snippet_length = mb_strlen(trim($snippet_content));
                           $extractinfo = phpdigGetExtracts($num_extracts,$reg_strings,$snippet_content,$query_string);
                           $extract .= $extractinfo[0];
                           $num_extracts = $extractinfo[1];
                           $where_to_start += $extractinfo[2];
                       }
                       $extract_size = phpdigExtractSize($extract_size,$size_for_while);
                    }
                }
             }

             $text = phpdigVerifyEnds(mb_substr($text,0,SUMMARY_DISPLAY_LENGTH))."...";
             $text = phpdigHighlight($reg_strings,mb_ereg_replace("([ ]{2,}|\n|\r|\r\n|\t)"," ",$text));

             if (mb_strlen($title) > TITLE_DISPLAY_LENGTH) {
                 $title = phpdigVerifyEnds(mb_substr($title,0,TITLE_DISPLAY_LENGTH))."...";
             }
             else {
                 $title = phpdigVerifyEnds($title);
             }
             $title = htmlspecialchars(phpdigHighlight($reg_strings,urldecode($title)),ENT_QUOTES);
             $title = phpdigSpanReplace($title);

             $timer->stop('Extracts');

             $table_results[$n] = array (
                    'weight' => $weight,
                    'img_tag' => '<img border="0" src="'.WEIGHT_IMGSRC.'" width="'.ceil(WEIGHT_WIDTH*$weight/100).'" height="'.WEIGHT_HEIGHT.'" alt="" />',
                    'page_link' => "<a class=\"phpdig\" href=\"".$url."\" onmousedown=\"return clickit(".$n.",'".$js_url."')\" target=\"".LINK_TARGET."\" >".$title."</a>",
                    'limit_links' => phpdigMsg('limit_to')." ".$l_site.$l_path,
                    'filesize' => sprintf('%.1f',(mb_ereg_replace('.*_([0-9]+)$','\1',$content['md5']))/1024),
                    'update_date' => mb_ereg_replace('^([0-9]{4})[-]?([0-9]{2})[-]?([0-9]{2}).*',PHPDIG_DATE_FORMAT,$content['last_modified']),
                    'complete_path' => $url,
                    'link_title' => $title
                    );

             $table_results[$n]['text'] = '';
             if (DISPLAY_SUMMARY) {
                 $table_results[$n]['text'] = htmlspecialchars($text,ENT_QUOTES);
                 $table_results[$n]['text'] = phpdigSpanReplace($table_results[$n]['text']);
             }
             if (DISPLAY_SUMMARY && DISPLAY_SNIPPETS) {
                 $table_results[$n]['text'] .= "\n<br/><br/>\n";
             }
             if (DISPLAY_SNIPPETS) {
                 if ($extract) {
                     $extract = htmlspecialchars($extract,ENT_QUOTES);
                     $extract = phpdigSpanReplace($extract);
                     $table_results[$n]['text'] .= $extract;
                 }
                 else if (!$table_results[$n]['text']){
                     $table_results[$n]['text'] = htmlspecialchars($text,ENT_QUOTES);
                     $table_results[$n]['text'] = phpdigSpanReplace($table_results[$n]['text']);
                 }
             }
        }
    }

    $timer->stop('Result table');
    $timer->start('Final strings');

    $url_bar = SEARCH_PAGE."?template_demo=$template_demo&amp;browse=1&amp;query_string=".urlencode($my_query_string_link)."$refine_url&amp;limite=$limite&amp;option=$option&amp;lim_start=";

    if ($lim_start > 0) {
        $previous_link = $url_bar.($lim_start-$limite);
        $nav_bar .= "<a class=\"phpdig\" href=\"$previous_link\" >&lt;&lt;".phpdigMsg('previous')."</a>&nbsp;&nbsp;&nbsp; \n";
    }
    $tot_pages = ceil($num_tot/$limite);
    $actual_page = $lim_start/$limite + 1;
    $page_inf = max(1,$actual_page - 5);
    $page_sup = min($tot_pages,max($actual_page+5,10));
    for ($page = $page_inf; $page <= $page_sup; $page++) {
      if ($page == $actual_page) {
           $nav_bar .= " <span class=\"phpdigHighlight\">$page</span> \n";
           $pages_bar .= " <span class=\"phpdigHighlight\">$page</span> \n";
      }
      else {
          $nav_bar .= " <a class=\"phpdig\" href=\"".$url_bar.(($page-1)*$limite)."\" >$page</a> \n";
          $pages_bar .= " <a class=\"phpdig\" href=\"".$url_bar.(($page-1)*$limite)."\" >$page</a> \n";
      }
    }

    if ($more_results == 1) {
        $next_link = $url_bar.($lim_start+$limite);
        $nav_bar .= " &nbsp;&nbsp;&nbsp;<a class=\"phpdig\" href=\"$next_link\" >".phpdigMsg('next')."&gt;&gt;</a>\n";
    }

    $mtime = explode(' ',microtime());
    $search_time = sprintf('%01.2f',$mtime[0]+$mtime[1]-$start_time);

    $result_message = stripslashes(ucfirst(phpdigMsg('results'))." $n_start-$n_end, $num_tot ".phpdigMsg('total').", $search_time ".phpdigMsg('seconds').", ".phpdigMsg('on')." &quot;".htmlspecialchars($query_string,ENT_QUOTES)."&quot;");

    $timer->stop('Final strings');
}
else {
    if (is_array($strings)) {
        $strings = array_values($strings);
        $num_in_strings_arr = count($strings);
    }
    else { $num_in_strings_arr = 0; }
    $leven_final = "";
    $leven_sum = 0;
    if (($num_in_strings_arr > 0) && (mb_strlen($path) == 0)) {
        for ($i=0; $i<$num_in_strings_arr; $i++) {
            $soundex_query = "SELECT keyword FROM ".PHPDIG_DB_PREFIX."keywords WHERE SOUNDEX(CONCAT(_utf8'Q',keyword)) = SOUNDEX(CONCAT('Q','".$strings[$i]."')) LIMIT 500";
            $soundex_results = mysql_query($soundex_query,$id_connect);
            if (mysql_num_rows($soundex_results) > 0) {
                 $leven_ind = 0;
                 $leven_amt1 = 256;
                 $leven_keyword = array();
                 while (list($soundex_keyword) = mysql_fetch_array($soundex_results)) {
                     $leven_amt2 = min(levenshtein(stripslashes($strings[$i]),$soundex_keyword),$leven_amt1);
                     if (($leven_amt2 < $leven_amt1) && ($leven_amt2 >= 0) && ($leven_amt2 <= 5)) {
                         $leven_keyword[$leven_ind] = stripslashes($soundex_keyword);
                         $leven_ind++;
                     }
                     $leven_amt1 = $leven_amt2;
                 }
                $leven_count = count($leven_keyword);
                $leven_sum = $leven_sum + $leven_amt1;
                if ($leven_count > 0) {
                    $leven_final .= $leven_keyword[$leven_count-1] . " ";
                }
                if (isset($leven_keyword)) { unset($leven_keyword); }
            }
        }
    }
    $num_tot = 0;
    $result_message = phpdigMsg('noresults');
    if ((mb_strlen(trim($leven_final)) > 0) && ($leven_sum > 0)) {
        $leven_query = trim($leven_final);
        $result_message .= ". " . phpdigMsg('alt_try') ." <a class=\"phpdigMessage\" href=\"".SEARCH_PAGE."?template_demo=$template_demo&amp;query_string=".urlencode($leven_query)."&amp;option=any\"><i>".htmlspecialchars($leven_query,ENT_QUOTES)."</i></a>?";
    }
}

if (isset($tempresult)) {
    mysql_free_result($tempresult);
}

$title_message = stripslashes(ucfirst(phpdigMsg('s_results'))." ".phpdigMsg('on')." ".htmlspecialchars($query_string,ENT_QUOTES));
}
else {
   $title_message = 'PhpDig '.PHPDIG_VERSION;
   $result_message = phpdigMsg('no_query').'.';
}

$timer->start('Logs');
if (PHPDIG_LOGS == true && !$browse && !$refine && $adlog_flag == 0) {
   if (is_array($final_result)) {
       phpdigAddLog ($id_connect,$option,$strings,$exclude,count($final_result),$search_time);
   }
   else {
       phpdigAddLog ($id_connect,$option,$strings,$exclude,0,$search_time);
   }
}
$timer->stop('Logs');

$timer->start('Template parsing');

$powered_by_link = "<font size=\"1\" face=\"verdana,arial,sans-serif\">";
if ((ALLOW_RSS_FEED == true) && (!defined('LIST_LINKS')) && ($num_tot > 0)) {
  $powered_by_link .= "<a href=\"".$rssdf."\">".phpdigMsg('viewRSS')."</a><br>";
}
if ((LIST_ENABLE == true) && (!defined('LIST_LINKS'))) {
  if (!empty($template_demo)) { $listplate = "template_demo=$template_demo"; } else { $listplate = ""; }
  $powered_by_link .= "<a href=\"".LIST_PAGE."?$listplate\">".phpdigMsg('viewList')."</a><br>";
}
$powered_by_link .= "<a href=\"http://www.phpdig.net/\">".phpdigMsg('powered_by')."</a><br></font>";

if (is_array($strings)) {
   $js_string = implode(" ",$strings);
} else {
   $js_string = "";
}
$js_for_clicks = "
<script language=\"JavaScript\">
<!--
function clickit(cn,clink) {
  if(document.images) {
     (new Image()).src=\"clickstats.php?num=\"+cn+\"&url=\"+clink+\"&val=".urlencode($js_string)."\";
  }
  return true;
}
//-->
</script>
";

if ($template == 'array' || is_file($template)) {
    $phpdig_version = PHPDIG_VERSION;
    $t_mstrings = compact('js_for_clicks','powered_by_link','title_message','phpdig_version','result_message','nav_bar','ignore_message','ignore_commess','pages_bar','previous_link','next_link');
    $t_fstrings = phpdigMakeForm($query_string,$option,$limite,SEARCH_PAGE,$site,$path,'template',$template_demo,$num_tot,$refine);
    if ($template == 'array') {
        return array_merge($t_mstrings,$t_fstrings,array('results'=>$table_results));
    }
    else {
        $t_strings = array_merge($t_mstrings,$t_fstrings);
        phpdigParseTemplate($template,$t_strings,$table_results);
    }
}
else {
?>
<?php include $relative_script_path.'/libs/htmlheader.php' ?>
<head>
<title><?php print $title_message ?></title>
<?php include $relative_script_path.'/libs/htmlmetas.php' ?>
<style>
.phpdigHighlight {color:<?php print HIGHLIGHT_COLOR ?>;
                 background-color:<?php print HIGHLIGHT_BACKGROUND ?>;
                 font-weight:bold;
                 }
.phpdigMessage {padding:1px;background-color:#002288;color:white;}
</style>
<script language="JavaScript">
<!--
function clickit(cn,clink) {
  if(document.images) {
    (new Image()).src="clickstats.php?num="+cn+"&url="+clink+"&val=<?php echo urlencode($js_string); ?>";
  }
  return true;
}
//-->
</script>
</head>
<body bgcolor="white">
<div align="center">
<img src="phpdig_logo_2.png" width="200" height="114" alt="phpdig <?php print PHPDIG_VERSION ?>" border="0" />
<br />
<?php
phpdigMakeForm($query_string,$option,$limite,SEARCH_PAGE,$site,$path,'classic',$template_demo,$num_tot,$refine);
?>
<h3><span class="phpdigMsg"><?php print $result_message ?></span>
<br /><span class="phpdigAlert"><?php print $ignore_message ?></span>
<br /><span class="phpdigAlert"><?php print $ignore_commess ?></span>
</h3>
</div>
<?php
if (is_array($table_results)) {
       while (list($n,$t_result) = each($table_results)) {
             print "<p style='background-color:#CCDDFF;'>\n";
             print "<b>$n. <font style='font-size:10;'>[".$t_result['weight']." %]</font>&nbsp;&nbsp;".$t_result['page_link']."</b>\n<br />\n";
             print "<font style='font-size:10;background-color:#BBCCEE;'>".$t_result['limit_links']."</font>\n<br />\n";
             print "</p>\n";
             print "<blockquote style='background-color:#EEEEEE;font-size:10;'>\n";
             print $t_result['text'];
             print "</blockquote>\n";
       }
}
print "<p style='text-align:center;background-color:#CCDDFF;font-weight:bold'>\n";
print $nav_bar;
print "</p>\n";
?>
<hr />
<div align="center">
<?php
if ($query_string) {
    phpdigMakeForm($query_string,$option,$limite,SEARCH_PAGE,$site,$path,'classic',$template_demo,$num_tot,$refine);
}
?>
</div>
<div align='center'>
<a href='http://www.phpdig.net/' target='_blank'><img src='phpdig_powered_2.png' width='88' height='28' border='0' alt='Powered by PhpDig' /></a> &nbsp;
</div>
</body>
</html>
<?php
}
$timer->stop('Template parsing');
$timer->stop('All display');
$timer->stop('All');
}

function phpdigSpanReplace($text) {
$text = mb_ereg_replace("&lt;/span&gt;","</span>",$text);
$text = mb_ereg_replace("&lt;span class=&quot;phpdigHighlight&quot;&gt;","<span class=\"phpdigHighlight\">",$text);
$text = str_replace("&amp;","&",$text);
return $text;
}

function phpdigByLength($a, $b) {
$len_a = mb_strlen($a);
$len_b = mb_strlen($b);
if ($len_a == $len_b) { return 0; }
return ($len_a < $len_b) ? 1 : -1;
}

function phpdigRegStrings($reg_strings) {
   // start extra multi-byte regex processing due to http://bugs.php.net/bug.php?id=25953
   // this extra regex processing tries to highlight word, WORD, and Word spelling variants
   $front = mb_eregi_replace("[(](.*)[)][(](.*)[)][(](.*)[)]","\\1",$reg_strings);
   $back = mb_eregi_replace("[(](.*)[)][(](.*)[)][(](.*)[)]","\\3",$reg_strings);
   $mid_value1 = mb_eregi_replace("[(](.*)[)][(](.*)[)][(](.*)[)]","\\2",$reg_strings);
   $mid_value2 = mb_strtoupper($mid_value1);
   $mid_value3 = $mid_value1;
   if ($mid_value2 != $mid_value1) {
      $mid_value1 .= "|".$mid_value2;
   }
   $mid_value3 = mb_strtoupper(mb_substr($mid_value3,0,1)).mb_substr($mid_value3,1);
   while (mb_eregi("([ ]([^ ]+))",$mid_value3,$regs3)) {
     $mid_value3 = mb_ereg_replace($regs3[1],"?_?".mb_strtoupper(mb_substr($regs3[2],0,1)).mb_substr($regs3[2],1),$mid_value3);
   }
   $mid_value3 = mb_ereg_replace("[?][_][?]"," ",$mid_value3);
   if ($mid_value3 != $mid_value1) {
     $mid_value1 .= "|".$mid_value3;
   }
   $middle = $mid_value1;
   $reg_strings = "(".$front.")(".$middle.")(".$back.")";
   // end extra multi-byte regex processing due to http://bugs.php.net/bug.php?id=25953
   return $reg_strings;
}

function phpdigGetExtracts($num_extracts,$reg_strings,$extract_content,$query_string) {
  $extract = '';
  $max_off = (int) min(mb_strlen($query_string)+1,floor(0.80*mb_strlen($extract_content)));
  $where_to_start = mb_strlen($extract_content) - $max_off;
  if(mb_eregi($reg_strings,$extract_content)) {
    $match_this_spot = mb_eregi_replace($reg_strings,"\\1<\\2>\\3",$extract_content);
    $first_bold_spot = mb_strpos($match_this_spot,"<");
    $first_bold_spot = (int) max($first_bold_spot - round((SNIPPET_DISPLAY_LENGTH / 2),0), 0);
    $length_of_snip = max(SNIPPET_DISPLAY_LENGTH, 2 * mb_strlen($query_string));
    $extract_content = mb_substr($extract_content,$first_bold_spot,$length_of_snip);
    $extract_content = phpdigVerifyEnds($extract_content);
    $extract = ' ...'.phpdigHighlight($reg_strings,$extract_content).'... ';
    $where_to_start = $first_bold_spot + $length_of_snip;
    $num_extracts++;
  }
  return array($extract,$num_extracts,$where_to_start);
}

function phpdigExtractSize($extract_size,$size_for_while) {
  if ($extract_size < $size_for_while) {
    $extract_size *= 100;
    if ($extract_size > $size_for_while) {
      $extract_size = $size_for_while;
    }
  }
  else {
    $extract_size++;
  }
  return $extract_size;
}

?>