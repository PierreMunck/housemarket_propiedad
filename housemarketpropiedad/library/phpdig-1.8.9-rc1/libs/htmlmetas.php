<?php
if (!defined('PHPDIG_ENCODING')) {
    die("Cannot display htmlmetas.php file.\n");
}
if (!isset($phpdig_language)) {
    $phpdig_language = PHPDIG_LANG_CONSTANT;
}
$phpdig_language_array = array('ca','cs','da','de','en','es','fr','gr','it','nl','no','pt','ru');
if (!in_array($phpdig_language,$phpdig_language_array)) {
    $phpdig_language = "en";
}
if (is_file("$relative_script_path/includes/style.css")) {
    $my_css_link = "$relative_script_path/includes/style.css";
}
else {
    die("Cannot find style.css file.\n");
}
?>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="<?php print $my_css_link ?>" rel="stylesheet" type="text/css" />