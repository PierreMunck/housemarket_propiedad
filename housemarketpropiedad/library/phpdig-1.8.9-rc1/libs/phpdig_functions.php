<?php
/*
----------------------------------------------------------------------------------
PhpDig Version 1.8.x - See the config file for the full version number.
This program is provided WITHOUT warranty under the GNU/GPL license.
See the LICENSE file for more information about the GNU/GPL license.
Contributors are listed in the CREDITS and CHANGELOG files in this package.
Developer from inception to and including PhpDig v.1.6.2: Antoine Bajolet
Developer from PhpDig v.1.6.3 to and including current version: Charter
Copyright (C) 2001 - 2003, Antoine Bajolet, http://www.toiletoine.net/
Copyright (C) 2003 - current, Charter, http://www.phpdig.net/
Contributors hold Copyright (C) to their code submissions.
Do NOT edit or remove this copyright or licence information upon redistribution.
If you modify code and redistribute, you may ADD your copyright to this notice.
----------------------------------------------------------------------------------
*/

define('CONFIG_CHECK','check'); // do not edit this line

//-------------UTILS FUNCTIONS

//=================================================
// extract _POST or _GET variables from a list varname => vartype
// Useful for error_reporting E_ALL too, init variables
// usage in script : extract(phpdigHttpVars(array('foobar'=>'string')));
function phpdigHttpVars($varray=array()) {
$parse_orders = array('_POST','_GET','HTTP_POST_VARS','HTTP_GET_VARS');
$httpvars = array();
// extract the right array
if (is_array($varray)) {
    foreach($parse_orders as $globname) {
          global $$globname;
          if (!count($httpvars) && isset($$globname) && is_array($$globname)) {
              $httpvars = $$globname;
          }
    }
    // extract or create requested vars
    foreach($varray as $varname => $vartype) {
       if (in_array($vartype,array('integer','bool','double','float','string','array')) ) {
         if (!isset($httpvars[$varname])) {
            if (!isset($GLOBALS[$varname])) {
                 $httpvars[$varname] = false;
            }
            else {
                 $httpvars[$varname] = $GLOBALS[$varname];
            }
         }
         settype($httpvars[$varname],$vartype);
       }
    }
return $httpvars;
}
}

//=================================================
// timer for profiling
class phpdigTimer {
      var $time = 0;
      var $mode = '';
      var $marks = array();
      var $template = '';

      function phpdigTimer($mode='html') {
           $this->time = $this->getTime();
           if ($mode == 'cli') {
               $this->template = "%s:\t%0.9f s. \n";
           }
           else {
               $this->template = "<tr><td class=\"greyForm\">%s</td><td class=\"greyForm\">%0.9f s. </td></tr>\n";
           }
      }
      function start($name) {
           if (!isset($this->marks[$name])) {
               $this->marks[$name]['time'] = $this->getTime();
               $this->marks[$name]['stat'] = 'r';
           }
           else if ($this->marks[$name]['stat'] == 's') {
               $this->marks[$name]['time'] = $this->getTime()-$this->marks[$name]['time'];
               $this->marks[$name]['stat'] = 'r';
           }
      }
      function stop($name) {
           if (isset($this->marks[$name]) && $this->marks[$name]['stat'] == 'r') {
               $this->marks[$name]['time'] = $this->getTime()-$this->marks[$name]['time'];
           }
           else {
               $this->marks[$name]['time'] = 0;
           }
           $this->marks[$name]['stat'] = 's';
      }
      function display() {
           if ($this->mode != 'cli') {
               print "<table class=\"borderCollapse\"><tr><td class=\"blueForm\">Mark</td><td class=\"blueForm\">Value</td></tr>\n";
           }
           foreach($this->marks as $name => $value) {
                printf($this->template,ucwords($name),$value['time']);
           }
           if ($this->mode != 'cli') {
               print "</table>\n";
           }
      }
      // increase precision with deltime
      function getTime() {
          return array_sum(explode(' ',microtime()))-$this->time;
      }
}

//-------------STRING FUNCTIONS

//=================================================
//returns a localized string
function phpdigMsg($string='') {
global $phpdig_mess;
if (isset($phpdig_mess[$string])) {
    return nl2br($phpdig_mess[$string]);
}
else {
    return ucfirst($string);
}
}

//=================================================
//print a localized string
function phpdigPrnMsg($string='') {
global $phpdig_mess;
if (isset($phpdig_mess[$string])) {
    print nl2br($phpdig_mess[$string]);
}
else {
    print ucfirst($string);
}
}

//=================================================
//load the common words in an array
function phpdigComWords($file='')
{
$lines = @file($file);
if (is_array($lines))
    {
    while (list($id,$word) = each($lines))
           $common[trim($word)] = 1;
    }
else
    $common['aaaa'] = 1;
return $common;
}

//=================================================
//highlight a string part
function phpdigHighlight($value='',$string='') {
if ($value) {

  // start extra multi-byte regex processing due to http://bugs.php.net/bug.php?id=25953
  // this extra regex processing tries to highlight word, WORD, and Word spelling variants
  $front = mb_eregi_replace("[(](.*)[)][(](.*)[)][(](.*)[)]","\\1",$value);
  $back = mb_eregi_replace("[(](.*)[)][(](.*)[)][(](.*)[)]","\\3",$value);
  $mid_value1 = mb_eregi_replace("[(](.*)[)][(](.*)[)][(](.*)[)]","\\2",$value);
  $mid_value2 = mb_strtoupper($mid_value1);
  $mid_words1 = explode("|",$mid_value1);
  $mid_words2 = explode("|",$mid_value2);
  $mid_words3 = $mid_words1;
  foreach ($mid_words2 as $val_words2) {
    if (!in_array($val_words2,$mid_words1)) {
       $mid_words1[] = $val_words2;
    }
  }
  foreach ($mid_words3 as $val_words3) {
    $val_words3 = mb_strtoupper(mb_substr($val_words3,0,1)).mb_substr($val_words3,1);
    while (mb_eregi("([ ]([^ ]+))",$val_words3,$regs3)) {
      $val_words3 = mb_ereg_replace($regs3[1],"?_?".mb_strtoupper(mb_substr($regs3[2],0,1)).mb_substr($regs3[2],1),$val_words3);
    }
    $val_words3 = mb_ereg_replace("[?][_][?]"," ",$val_words3);
    if (!in_array($val_words3,$mid_words1)) {
      $mid_words1[] = $val_words3;
    }
  }
    $middle = implode("|",$mid_words1);
    $value = "(".$front.")(".$middle.")(".$back.")";
    // end extra multi-byte regex processing due to http://bugs.php.net/bug.php?id=25953

    //$string = @mb_eregi_replace($value,"\\1<^#_>\\2</_#^>\\3",@mb_eregi_replace($value,"\\1<^#_>\\2</_#^>\\3",$string));
    $string = @mb_eregi_replace($value,"\\1<^#_>\\2</_#^>\\3",$string);
    $string = str_replace("^#_","span class=\"phpdigHighlight\"",str_replace("_#^","span",$string));

    return $string;
  }
  else {
    return $string;
  }
}

//=================================================
//epure a string from all non alnum words (words can contain certain characters)
function phpdigEpureText($text,$min_word_length=2,$encoding=PHPDIG_ENCODING) {

$text = mb_ereg_replace("^([\x00-\x1f]|[\x21-\x2f]|[\x3a-\x40]|[\x5b-\x60]|[\x7b-\x7f])+","",$text); //off front only
$text = mb_ereg_replace("([\x00-\x1f]|[\x21-\x2f]|[\x3a-\x40]|[\x5b-\x60]|[\x7b-\x7f])+$","",$text); //off back only

// the next two repeated lines needed
if ($min_word_length >= 1) {
  $text = mb_ereg_replace('[[:space:]][^ ]{1,'.$min_word_length.'}[[:space:]]',' ',' '.$text.' ');
  $text = mb_ereg_replace('[[:space:]][^ ]{1,'.$min_word_length.'}[[:space:]]',' ',' '.$text.' ');
}

$text = mb_ereg_replace('\.{2,}',' ',$text);
$text = mb_ereg_replace('^[[:space:]]*\.+',' ',$text);

return trim(mb_ereg_replace("[[:space:]]+"," ",$text));
}

//-------------UTF-8 FUNCTIONS

function phpdigVerifyUTF8($str) {
  // verify if a given string is encoded in valid utf-8
  if ($str === mb_convert_encoding(mb_convert_encoding($str, "UTF-32", "UTF-8"), "UTF-8", "UTF-32")) {
    return $str;
  }
  else {
    return false;
  }
}

function phpdigVerifyEnds($str) {
  $str = trim($str);
  if (mb_eregi(" ",$str)) {
    $str_array  = explode(" ",$str);
    $str_count = count($str_array);
    if ($str_count > 2) {
      if (phpdigVerifyUTF8($str_array[0]) == false) {
        array_shift($str_array);
        $str_count = count($str_array);
      }
      if (phpdigVerifyUTF8($str_array[$str_count-1]) == false) {
        array_pop($str_array);
      }
    }
    $str  = implode(" ",$str_array);
  }
  return $str;
}

//-------------SQL FUNCTIONS

//=================================================
//insert an entry in logs
function phpdigAddLog($id_connect,$option='start',$includes=array(),$excludes=array(),$num_results=0,$time=0) {
    if (!is_array($excludes)) {
         $excludes = array();
    }
    sort($excludes);
    if (!is_array($includes)) {
         $includes = array();
    }
    //sort($includes);
    $query = 'INSERT INTO '.PHPDIG_DB_PREFIX.'logs (l_num,l_mode,l_ts,l_includes,l_excludes,l_time) '
             .'VALUES ('.$num_results.',\''.mb_substr($option,0,1).'\',NOW(),'
             .'\''.implode(' ',$includes).'\',\''.implode(' ',$excludes).'\','.(double)$time.')';
    mysql_query($query,$id_connect);
    return mysql_insert_id($id_connect);
}

?>