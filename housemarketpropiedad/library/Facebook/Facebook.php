<?php
/**
* Copyright 2011 Facebook, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may
* not use this file except in compliance with the License. You may obtain
* a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/

require_once "BaseFacebook.php";

/**
* Extends the BaseFacebook class with the intent of using
* PHP sessions to store user ids and access tokens.
*/
class Facebook_Facebook extends Facebook_BaseFacebook
{

  public $log;
  private $tmpUserAccessToken = null;
  private $tmpPageAccessToken = null;
  
  /**
* Identical to the parent constructor, except that
* we start a PHP session to store the user ID and
* access token if during the course of execution
* we discover them.
*
* @param Array $config the application configuration.
* @see BaseFacebook::__construct in facebook.php
*/
  public function __construct($config) {
    if (!session_id()) {
      session_start();
    }
    if(Zend_Registry::isRegistered('log')){
    	$this->log = Zend_Registry::get('log');
    }
    parent::__construct($config);
  }

  protected static $kSupportedKeys =
    array('state', 'code', 'access_token', 'user_id');

  /**
* Provides the implementations of the inherited abstract
* methods. The implementation uses PHP sessions to maintain
* a store for authorization codes, user ids, CSRF states, and
* access tokens.
*/
  protected function setPersistentData($key, $value) {
    if (!in_array($key, self::$kSupportedKeys)) {
      self::errorLog('Unsupported key passed to setPersistentData.');
      return;
    }

    $session_var_name = $this->constructSessionVariableName($key);
    $_SESSION[$session_var_name] = $value;
  }

  protected function getPersistentData($key, $default = false) {
    if (!in_array($key, self::$kSupportedKeys)) {
      self::errorLog('Unsupported key passed to getPersistentData.');
      return $default;
    }

    $session_var_name = $this->constructSessionVariableName($key);
    return isset($_SESSION[$session_var_name]) ?
      $_SESSION[$session_var_name] : $default;
  }

  protected function clearPersistentData($key) {
    if (!in_array($key, self::$kSupportedKeys)) {
      self::errorLog('Unsupported key passed to clearPersistentData.');
      return;
    }

    $session_var_name = $this->constructSessionVariableName($key);
    unset($_SESSION[$session_var_name]);
  }

  protected function clearAllPersistentData() {
    foreach (self::$kSupportedKeys as $key) {
      $this->clearPersistentData($key);
    }
  }

  protected function constructSessionVariableName($key) {
    return implode('_', array('fb',
                              $this->getAppId(),
                              $key));
  }
  
  public function safecheck($code = null){
  	try {
  		// need to circumvent json_decode by calling _oauthRequest
  		// directly, since response isn't JSON format.
  		$access_token_response =
  		$this->_oauthRequest(
  				$this->getUrl('graph', '/oauth/access_token'),
  				$params = array(
					'client_id' => $this->getAppId(),
					'client_secret' => $this->getAppSecret(),
					'redirect_uri' => $this->getCurrentUrl(),
					'code' => $code,
  					)
  				);
  	} catch (FacebookApiException $e) {
  		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
  		// most likely that user very recently revoked authorization.
  		// In any event, we don't have an access token, so say so.
  		return false;
  	}
  	
  	$response_params = array();
  	parse_str($access_token_response, $response_params);
  	if (!isset($response_params['access_token'])) {
  		return false;
  	}
  	$this->accessToken = $response_params['access_token'];
  	$user = $this->api('/me');
  	$user['code'] = $code;
  	$user['user_id'] = $user['id'];
  	$user['algorithm'] = 'HMAC-SHA256';
  	$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($user,true));
  	$this->signedRequest = $user;
  }
  
  public function setUserAccessToken(){
  	if(isset($this->tmpUserAccessToken)){
  		$this->accessToken = $this->tmpUserAccessToken;
  	}
  	//get an access token user appid secretid
  }
  
  public function setPageAccessToken($uid,$id_page = null){
  	if(!isset($this->tmpPageAccessToken[$id_page]) && isset($id_page)){
  		$url = '/'. $uid .'/accounts';
  		$page_accounts = $this->api($url);
  		foreach($page_accounts['data'] as $page){
  			$this->tmpPageAccessToken[$page['id']] = $page['access_token'];
  		}
  	}
  	if($this->accessToken != $this->tmpPageAccessToken[$id_page]){
  		$this->tmpUserAccessToken = $this->accessToken;
  	}
  	$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($this->tmpPageAccessToken,true));
  	$this->accessToken = $this->tmpPageAccessToken[$id_page];
  	$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($this->accessToken,true));
  }
}