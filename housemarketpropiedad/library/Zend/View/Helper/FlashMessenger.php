<?php

class Zend_View_Helper_FlashMessenger extends Zend_View_Helper_FormElement
{
	private $_types = array(
	Zend_Controller_Action_Helper_ZsamerFlashMessenger::ERROR,
	Zend_Controller_Action_Helper_ZsamerFlashMessenger::WARNING,
	Zend_Controller_Action_Helper_ZsamerFlashMessenger::NOTICE,
	Zend_Controller_Action_Helper_ZsamerFlashMessenger::SUCCESS
                                 
	);

        /**
         *
         * @return String HTML que representa
         */
	public function flashMessenger()
	{
		$flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('ZsamerFlashMessenger');
		$html = '';
		foreach ($this->_types as $type) {
			$messages = $flashMessenger->getMessages($type);
			if (!$messages){
				$messages = $flashMessenger->getCurrentMessages($type);
			}
			if ($messages) {
				$html .= '<div class="' . $type . '">';
				$html .= '<ul>';
				foreach ( $messages as $message ) {
					$html.= '<li>';
					$html.= $message->message;
					$html.= '</li>';
				}
				$html .= '</ul>';
				$html .= '</div>';
			}
		}

		return $html;
	}

        /*
	public function flashMessenger()
	{
		$flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('ZsamerFlashMessenger');
		$html = '';
		foreach ($this->_types as $type) {
			$messages = $flashMessenger->getMessages($type);
			if (!$messages){
				$messages = $flashMessenger->getCurrentMessages($type);
			}
			if ($messages) {
				if ( !$html ) {
					$html .= '<ul class="messages">';
				}
				$html .= '<li class="' . $type . '-msg">';
				$html .= '<ul>';
				foreach ( $messages as $message ) {
					$html.= '<li>';
					$html.= $message->message;
					$html.= '</li>';
				}
				$html .= '</ul>';
				$html .= '</li>';
			}
		}
		if ( $html) {
			$html .= '</ul>';
		}
		return $html;
	}
         */
}

?>