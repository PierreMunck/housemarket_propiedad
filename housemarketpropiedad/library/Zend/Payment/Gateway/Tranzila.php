<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Payment
 * @subpackage Gateway
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id:  $
 */
 
 
/**
 * @see Zend_Payment
 */
require_once 'Zend/Payment.php';
 
 
/**
 * Class for online Ecommerce charging/payments
 *
 * @category   Zend
 * @package    Zend_Payment
 * @subpackage Gateway
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id:  $
 */
class Zend_Payment_Gateway_Tranzila extends Zend_Payment_Gateway_Abstract
{
 
    /**
     * Required options per gateways, things such as suplier, merchant account id
     * email addresess
     *
     * Other:
     *       'gatewayNamespace' => custom namespace for a gateway
     *
     * @var array
     */
    protected $_options = array(
 
    'supplier' => null, // supplier name
    'uri' => 'https://secure5.tranzila.com/cgi-bin/tranzila31.cgi', // gateway uri
 
    );
 
    /**
     * Constructor.
     *
     * $options is an array of key/value pairs or an instance of Zend_Config
     * containing configuration options.
     *
     * @param  array|Zend_Config $options An array or instance of Zend_Config having configuration data
     * @throws Zend_Payment_Exception
     */
    public function __construct($options)
    {
        parent::__construct($options);
    }
 
    /**
     * Get a field from the fields array
     *
     * @param string|boolean|array|int $name
     */
    public function getField($name)
    {
        return array_key_exists($name, $this->_fields) ? $this->_fields[$name] : null;
    }
 
    /**
     * Add a key=>value pair to the fields stack
     *
     * @param string $name
     * @param string|boolean|array|int $value
     * @return object
     */
    public function setField($name, $value)
    {
        $this->_fields[$name] = $value;
        return $this;
    }
 
    /**
     * Magic method to set a field
     *
     * @param string $name
     * @param string|boolean|array|int $value
     * @return $value
     */
    public function __set($name, $value)
    {
        return $this->setField($name, $value);
    }
 
    /**
     * Magic method to get a value of a fied by it's name
     *
     * @param  string $name
     */
    public function __get($name)
    {
        return $this->getField($name);
    }
 
    /**
     * Returns the configuration variables in this gateway.
     * Or a single config var if the $value passed
     *
     * @param string $value
     * @return array|string on success exception on failure
     */
    public function getConfig($value=null)
    {
        if($value)
        {
            if(array_key_exists($value, $this->_options))
            {
                return $this->_options[$value];
            }
 
            throw new Zend_Payment_Gateway_Exception("The value {$value} was not found in the class config array");
        }
 
        return $this->_options;
    }
 
    /**
     * Abstract Methods
     */
 
 
    /**
     * Check for config options that are mandatory.
     *
     * @throws Zend_Payment_Gateway_Exception
     */
    public function _checkRequiredOptions()
    {
        if(!$this->_options['supplier'] || !$this->_options['password'])
        {
            throw new Zend_Payment_Gateway_Exception("Supplier & Password must be specifed");
        }
    }
 
    /**
     * Do process
     *
     */
    public function process()
    {
        // using curl or something simililar to process
    }
 
}