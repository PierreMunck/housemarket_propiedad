<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Payment
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: $
 */
 
/**
 * @see Zend_Loader
 */
require_once 'Zend/Loader.php';
 
/**
 * @category   Zend
 * @package    Zend_Payment
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id:  $
 */
class Zend_Payment
{
 
    /**
     * Factory for Zend_Payment_Gateway_Abstract classes.
     *
     * The first argument is a string that will represent the name
     * of the gateway class to use, For example: 'Paypal' corresponds to the class
     * Zend_Payment_Gateway_Paypal. This is case-insensitive.
     *
     * Second argument is optional and may be an associative array of key-value
     * pairs.  This is used as the argument to the gateway constructor.
     *
     * @param  mixed $gateway String name of gateway class.
     * @param  mixed $config  OPTIONAL; an array or Zend_Config object with parameters.
     * @return Zend_Payment_Gateway_Abstract
     * @throws Zend_Payment_Exception
     */
    public static function factory($gateway, $config = array())
    {
        if ($config instanceof Zend_Config) {
            $config = $config->toArray();
        }
 
        /*
        * Verify that gateway parameters are in an array.
        */
        if (!is_array($config)) {
            /**
             * @see Zend_Payment_Exception
             */
            require_once 'Zend/Payment/Exception.php';
            throw new Zend_Payment_Exception('Gateway parameters must be in an array or a Zend_Config object');
        }
 
        /*
        * Verify that an adapter name has been specified.
        */
        if (!is_string($gateway) || empty($gateway)) {
            /**
             * @see Zend_Payment_Exception
             */
            require_once 'Zend/Payment/Exception.php';
            throw new Zend_Payment_Exception('Gateway name must be specified in a string');
        }
 
        /*
        * Form full gateway class name
        */
        $gatewayNamespace = 'Zend_Payment_Gateway';
        if (isset($config['gatewayNamespace'])) {
            if ($config['gatewayNamespace'] != '') {
                $gatewayNamespace = $config['gatewayNamespace'];
            }
            unset($config['gatewayNamespace']);
        }
        $gatewayName = strtolower($gatewayNamespace . '_' . $gateway);
        $gatewayName = str_replace(' ', '_', ucwords(str_replace('_', ' ', $gatewayName)));
 
        /*
        * Load the adapter class.  This throws an exception
        * if the specified class cannot be loaded.
        */
        Zend_Loader::loadClass($gatewayName);
 
        /*
        * Create an instance of the adapter class.
        * Pass the config to the adapter class constructor.
        */
        $paymentGateway = new $gatewayName($config);
 
        /*
        * Verify that the object created is a descendent of the abstract gateway type.
        */
        if (! $paymentGateway instanceof Zend_Payment_Gateway_Abstract) {
            /**
             * @see Zend_Payment_Exception
             */
            require_once 'Zend/Payment/Exception.php';
            throw new Zend_Payment_Exception("Gateway class '$gatewayName' does not extend Zend_Payment_Gateway_Abstract");
        }
 
        return $paymentGateway;
    }
}