<?php
class Hm_View_Pager{
	private $size = 0;
	private $current = 0;
	private $url = '';
	private $amplitud = 10;
	private $start = 0;
	private $end = 0;
	
	public function __construct($url,$current,$size){
		$this->url = $url;
		$this->current = $current;
		$this->size = $size;
		if($this->current < ($this->amplitud / 2)){
			$this->start = 1;
			if($this->amplitud < $this->size){
				$this->end = $this->amplitud;
			}else{
				$this->end = $this->size;
			}
		}elseif($this->current > ($this->size - $this->amplitud)){
			$this->start = ($this->size - $this->amplitud);
			$this->end = $this->size;
		}else{
			$this->start = ($this->current - ($this->amplitud/2));
			if(($this->current + ($this->amplitud/2) - 1) < $this->size){
				$this->end = ($this->current + ($this->amplitud/2) - 1);
			}else{
				$this->end = $this->size;
			}
		}
	}
   
   public function __toString() {
   	$output = '';
   	$output .= "\n";
   	if(($this->start -1) > 0){
   		$output .= '<a href="'. $this->url .'&pager=1" class="pager_button pager_first">';
   		$output .= 'first' ;
   		$output .= '</a>' ;
   	}
   	if(($this->current -1) > 0){
   		$output .= '<a href="'. $this->url .'&pager='. ($this->current -1) .'" class="pager_button pager_prev">';
   		$output .= 'prev' ;
   		$output .= '</a>' ;
   	}
   	for($i = $this->start; $i <= $this->end; $i++){
   		$class = '';
   		if($i == $this->current){
   			$class = 'pager_current';
   		}
   		$output .= '<a href="'. $this->url .'&pager='. $i .'" class="'. $class .'">';
   		$output .= $i ;
   		$output .= '</a>' ;
   		$output .= "\n";
   	}
   	if(($this->current +1) < $this->size){
	   	$output .= '<a href="'. $this->url .'&pager='. ($this->current +1) .'" class="pager_button pager_next">';
	   	$output .= 'next' ;
	   	$output .= '</a>' ;
	   	
   	}
   	if(($this->end +1) < $this->size){
   		$output .= '<a href="'. $this->url .'&pager='. $this->size .'" class="pager_button pager_last">';
   		$output .= 'last' ;
	   	$output .= '</a>' ;
	   	$output .= "\n";
   	}
   	return $output;
   }
   
}