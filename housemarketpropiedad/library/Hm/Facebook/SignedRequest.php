<?php

class Hm_Facebook_SignedRequest {
	
	public $log;
	/**
	 * Factor de conversion de pies cuadrados a metros cuadrados
	 */
	private $Fb_perm_all_user_info = array(
			'user_about_me',
			'email',
			//'user_activities',
			'user_birthday',
			//'user_checkins', // remove in "Include Checkins with Statuses" migration
			//'user_education_history',
			//'user_events',
			//'user_games_activity',
			'user_groups',
			'user_hometown',
			//'user_interests',
			//'user_likes',
			'user_location',
			//'user_notes',
			//'user_online_presence',
			//'user_photo_video_tags',
			//'user_photos',
			//'user_questions',
			//'user_relationship_details',
			'user_relationships',
			//'user_religion_politics',
			//'user_status',
			//'user_subscriptions',
			//'user_videos',
			'user_website',
			'user_work_history',
			);
	
	private $Fb_perm_all_friends_info = array(
			'friends_about_me',
			'friends_activities',
			'friends_birthday',
			//'friends_checkins', // remove in "Include Checkins with Statuses" migration
			'friends_education_history',
			'friends_events',
			'friends_games_activity',
			'friends_groups',
			'friends_hometown',
			'friends_interests',
			'friends_likes',
			'friends_location',
			'friends_notes',
			'friends_online_presence',
			'friends_photo_video_tags',
			'friends_photos',
			'friends_questions',
			'friends_relationship_details',
			'friends_relationships',
			'friends_religion_politics',
			'friends_status',
			'friends_subscriptions',
			'friends_videos',
			'friends_website',
			'friends_work_history',
	);
	
	private $signedRequest = null;
	private $criptSignedRequest = null;
	
	private $urlAccessRules = array();
	
	public function __construct($signed_request) {
		if(Zend_Registry::isRegistered('log')){
			$this->log = Zend_Registry::get('log');
		}
		$this->urlAccessRules = array(
				'index' => array(
						'_default' => true
				),
				'register' => array(
						'index' => array('user_about_me','email','manage_pages','publish_stream'),
						'post' => array('user_about_me','email','manage_pages','publish_stream'),
						'validate' => array('user_about_me','email','manage_pages','publish_stream'),
						'finalizar' => true,
				),
				'landing' => array(
						'_default' => true,
				),
				'contactar' => array(
						'_default' => true,
						'post' => true,
				),
				'agregar' => array(
						'_default' => array_merge($this->Fb_perm_all_user_info,array('publish_stream')),
						'propiedad' => array_merge($this->Fb_perm_all_user_info,array('publish_stream')),
				),
				'show' => true,
				'migrate' => array(
						'index' => array('user_about_me','email','manage_pages','publish_stream'),
						'execute' => array('user_about_me','email','manage_pages','publish_stream'),
						'run' => array('user_about_me','email','manage_pages','publish_stream'),
						),
				'application' => true,
				'propiedad' => array(
						'_default' => $this->Fb_perm_all_user_info,
						'like' => $this->Fb_perm_all_user_info,
						'unlike' => $this->Fb_perm_all_user_info,
				),
		);
		
		if(is_array($signed_request)){
			$this->signedRequest = $signed_request;
		}else{
			$this->criptSignedRequest = $signed_request;
		}
	}
	
	public function getInfo() {
		if(is_array($this->signedRequest)){
			return $this->signedRequest;
		}
		
		$cfg = Zend_Registry::get('config');
		$secret = $cfg['facebook_secret'];
	
		list($encoded_sig, $payload) = explode('.', $this->criptSignedRequest, 2);
		// decode the data
		$sig = $this->base64_url_decode($encoded_sig);
		$data = json_decode($this->base64_url_decode($payload), true);
		if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
			error_log('Unknown algorithm. Expected HMAC-SHA256');
			return null;
		}
	
		// check sig
		$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
		if ($sig !== $expected_sig) {
			error_log('Bad Signed JSON signature!');
			return null;
		}
	
		$this->signedRequest = $data;
		return $this->signedRequest;
	}
	
	private function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_', '+/'));
	}
	
	/**
	 * function que permite verificar que el usuario puede acceder a la pagina sin mas permision
	 * @param Zend_param $request_params
	 * @return boolean
	 */
	public function check($request_params) {
		$controller = $request_params['controller'];
		$action = $request_params['action'];

		if(isset($this->urlAccessRules[$controller])){
			if($this->urlAccessRules[$controller] === true){
				return true;
			}
			if(is_array($this->urlAccessRules[$controller])){
				if(isset($this->urlAccessRules[$controller][$action])){
					if($this->urlAccessRules[$controller][$action] === true){
						return true;
					}
					if(is_array($this->urlAccessRules[$controller][$action])){
						//chek access this the type of permision
						/*if($this->signedRequest === null){
							return false;
						}*/
						$rules = $this->urlAccessRules[$controller][$action];
						if(!empty($rules)){
							if($this->Haspermision($rules)){
								// if necesita info y que no esta pedirle mas tarde
								//$this->getUserInfo();
								return true;
							}
						}
					}
				}
				if(isset($this->urlAccessRules[$controller]['_default']) && $this->urlAccessRules[$controller]['_default'] === true){
					return true;
				}
			}
		}
		return false;
	}
	
	public function getUserInfo() {
		try{
			$app = Zend_Registry::get('Facebook_App');
			$user = $app->api('/me');
			foreach ($user as $key => $value){
				$this->signedRequest[$key] = $value;
			}
		}catch(Exception $e){
			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
		}
	}
	
	public function getLocation() {
		if(isset($this->signedRequest['current_location']) && is_array($this->signedRequest['current_location'])){
			return $this->signedRequest['current_location'];
		}
		$app = Zend_Registry::get('Facebook_App');
		$fql = "SELECT+current_location+FROM+user+WHERE+uid=". $this->signedRequest['user_id'];
		try{
			$return = $app->api('/fql?q='.$fql);
		}catch (Exception $e){
			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
		}
		if(isset($return['data'][0]) && isset($return['data'][0]['current_location'])){
			$this->signedRequest['current_location'] = $return['data'][0]['current_location'];
			return $this->signedRequest['current_location'];
		}else{
			return false;
		}
	}
	
	public function getPaginas() {
		if(isset($this->signedRequest['admin_page_list']) && is_array($this->signedRequest['admin_page_list'])){
			return $this->signedRequest['admin_page_list'];
		}
		$app = Zend_Registry::get('Facebook_App');
		$fql = "SELECT+page_id,name+FROM+page+WHERE+page_id+IN+(+SELECT+page_id+FROM+page_admin+WHERE+uid=". $this->signedRequest['user_id'] ."+)";
		try{
			$return = $app->api('/fql?q='.$fql);
		}catch (Exception $e){
			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
		}
		if(isset($return['data']) && !empty($return['data'])){
			$this->signedRequest['admin_page_list'] = array();
			foreach($return['data'] as $page){
				$page_id = sprintf('%0.0f',$page['page_id']);
				$this->signedRequest['admin_page_list'][$page_id] = $page['name'];
			} 
			return $this->signedRequest['admin_page_list'];
		}else{
			return false;
		}
	}
	
	public function Haspermision($rules) {
		// use cache if posible
		if(!isset($this->signedRequest['user_id'])){
			return false;
		}
		if(empty($rules)){
			return false;
		}
		$app = Zend_Registry::get('Facebook_App');
		$PermFQL = implode(',',$rules);
		$fql = "SELECT+". $PermFQL ."+FROM+permissions+WHERE+uid+=+". $this->signedRequest['user_id'];
		try{
			$return = $app->api('/fql?q='.$fql);
		}catch (Exception $e){
			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
		}
		$permissionFb = array();
		if(isset($return['data'][0])){
			$permissionFb = $return['data'][0];
		}else{
			return false;
		}
		foreach ($permissionFb as $perm => $hasPerm){
			if(!$hasPerm){
				//$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($perm,true));
				return false;
			}
		}
		return true;
	}
	
	public function getAccessRule($request_params) {
		if(isset($this->urlAccessRules[$request_params['controller']])
				&& isset($this->urlAccessRules[$request_params['controller']][$request_params['action']]) ){
			return $this->urlAccessRules[$request_params['controller']][$request_params['action']];
		}
		return false;
	}
	
	public function get($info) {
		if(isset($this->signedRequest[$info])){
			return $this->signedRequest[$info];
		}
		return false;
	}
	
	public function getArray(){
		if(isset($this->signedRequest)){
			return $this->signedRequest;
		}
		return false;
	}
	
	public function set($info,$value) {
		if(isset($info) && isset($value)){
			$this->signedRequest[$info] = $value;
		}
		return false;
	}
	
	public function gethashSigned(){
		return $this->signedRequest;
	}
	
	public function pushwall($message){
		$app = Zend_Registry::get('Facebook_App');
		try {
			$post=array('cb' => '');
			if(isset($message->body)){
				$post['message']=$message->body;
			}
			if(isset($message->picture)){
				$post['picture']=$message->picture;
			}
			if(isset($message->link)){
				$post['link']=$message->link;
			}
			if(isset($message->name)){
				$post['name']=$message->name;
			}
			if(isset($message->caption)){
				$post['caption']=$message->caption;
			}
			if(isset($message->description)){
				$post['description']=$message->description;
			}
			$statusUpdate = $app->api('/me/feed', 'post', $post);
		} catch (FacebookApiException $e) {
			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
		}
	}
}