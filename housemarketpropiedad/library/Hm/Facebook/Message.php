<?php 

class Hm_Facebook_Message {
	
	public $body = null;
	public $picture = null;
	public $link = null;
	public $name = null;
	public $caption = 'housemarket';
	public $description = null;
	
	public function __construct($message = null) {
		if(is_string($message)){
			$this->body = $message;
		}
	}
	
	public function toarray(){
		$return = array('cb' => '');
		if(isset($message->body)){
			$return['message']=$message->body;
		}
		if(isset($message->picture)){
			$return['picture']=$message->picture;
		}
		if(isset($message->link)){
			$return['link']=$message->link;
		}
		if(isset($message->name)){
			$return['name']=$message->name;
		}
		if(isset($message->caption)){
			$return['caption']=$message->caption;
		}
		if(isset($message->description)){
			$return['description']=$message->description;
		}
		return $return;
	}
}
?>
