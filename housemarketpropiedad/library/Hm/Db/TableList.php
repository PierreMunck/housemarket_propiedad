<?php

/**
 * Controller por defecto
 */
class Hm_Db_TableList extends Zend_Db_Table implements Iterator{
	
	public $List = array();
	
	private $_position = 0;
	
	protected $_ObjectClass = null;
	
	protected $filtros = array();
	
	protected $filtroAgregado = array();
	
	protected $TotalResult = 0;
	
	protected $orderType = 'DESC';
	
	public $request = null;
	
	
	function __construct() {
		$this->_setAdapter('db');
		$this->_position = 0;
	}
	
	/**
	 * Methodo del iterator
	 */
	
	function rewind() {
		$this->_position = 0;
	}
	
	function get($position) {
		if(empty($this->List)){
			return false;
		}
		return $this->List[$position];
	}
	
	function current() {
		return $this->List[$this->_position];
	}
	
	function key() {
		return $this->_position;
	}
	
	function next() {
		++$this->_position;
	}
	
	function valid() {
		return isset($this->List[$this->_position]);
	}
	
	public function size() {
		return count($this->List);
	}
	
	public function totalResult() {
		return $this->TotalResult;
	}
	
	protected function modifySelect(&$select){
		
	}
	
	public function addfiltro($columna, $value, $operador = null, $table = null){
		if(is_null($operador)){
			$operador = '=';
		}
		if(is_null($table)){
			$class = new $this->_ObjectClass();
			$table = $class->getTableName();
		}
		
		$this->filtros[] = array(
				'table' => $table,
				'columna' => $columna,
				'operador' => $operador,
				'value' => $value,
				);
	}
	/**
	 * carga el objecto con el resltado de una seleccion
	 * @param unknown_type $paramField
	 */
	public function search($paramField = null, $modereturn = false, $limit = null, $order = null) {
		if(!isset($paramField) && !is_array($paramField)){
			return false;
		}
		
		$class = new $this->_ObjectClass();
		
		$des = $class->TableDescribe();
		
		if(!$this->checkValuequery($paramField,$class)){
			return false;
		}
		
		$select = $class->select();
		$select->setIntegrityCheck(false);
		$select->from($class->getTableName());
		// select by field
		foreach($paramField as $fieldKey => $fieldVal){
			if(is_array($fieldVal)){
				$vallist = array();
				foreach($fieldVal as $val){
					$vallist[] = $class->safeval($val,$des[$fieldKey]);
				}
				$fieldVal = $vallist;
				$select->where( $class->getTableName() .'.'. $fieldKey .' IN (?) ', $fieldVal);
			}else{
				$fieldVal = $class->safeval($fieldVal,$des[$fieldKey]);
				$select->where( $class->getTableName() .'.'. $fieldKey .' = ? ', $fieldVal);
			}
		}
		
		//depend table
		if(is_array($class->getDependentTables())){
			foreach($class->getDependentTables() as $depTable => $depRules){
				if($depRules['type'] == 'left'){
					$select->joinLeft($depTable, $depRules['value']);
				}
				if($depRules['type'] == 'strict'){
					$select->join($depTable, $depRules['value']);
				}
				if(isset($depRules['where'])){
					$select->where( $depTable .'.'. $depRules['where']['field'] .' = ? ', $depRules['where']['value']);
				}
			}
		}
		
		// filtro
		if(!empty($this->filtros)){
			foreach($this->filtros as $filtro){
				$where = '';
				if(isset($filtro['table'])){
					$where .= $filtro['table'] .'.';
				}
				if($filtro['value'] == "NOT NULL"){
					$where .= $filtro['columna'] .' IS NOT NULL ';
					$select->where( $where);
				}elseif ($filtro['value'] == "NULL"){
					$where .= $filtro['columna'] .' IS NULL' ;
					$select->where( $where);
				}else {
					if(is_array($filtro['value'])){
						$wherelist = array();
						foreach($filtro['value'] as $val){
							$wherelist[] = $where . $filtro['columna'] .' '. $filtro['operador'] .' \''. $val .'\' ';
						}
						$where = implode(' OR ', $wherelist);
						$select->where( $where);
					}else{
						$where .= $filtro['columna'] .' '. $filtro['operador'] .' ?';
						$select->where( $where, $filtro['value']);
					}
				}
			}
		}
		
		// filtro especial
		if(!empty($this->filtroAgregado)){
			foreach($this->filtroAgregado as $filtro){
				$select->where( $filtro );
			}
		}
		$this->modifySelect($select);
		
		
		// gestion de limit
		if(isset($limit)){
			if(is_int($limit)){
				if($limit != 0){
					$select->limit($limit);
				}
			}else{
				unset($limit);
			}
		}
		
		if(!isset($limit) && isset($this->limit)){
			if(is_int($this->limit) && $this->limit != 0){
				$select->limit($this->limit);
			}
		}
		
		// gestion de order
		
		if(!isset($order) && isset($this->orderType)){
			$primary = $class->getPrimaryKey();
			if(is_array($primary)){
				$order = array();
				foreach($primary as $key){
					$order[] = $class->getTableName() .'.'. $key.' '.$this->orderType;
				}
				$select->order($order);
				//order by primary list
			}else{
				$select->order(array($class->getTableName() .'.'. $primary.' '.$this->orderType));
			}
		}
		
		//debug querry
		$this->request = $select->assemble();
		
		//execute query
		$rs = $this->fetchAll($select);
		if(empty($rs)){
			return false;
		}
		$result = $rs->toArray();
		$this->TotalResult = count($result);
		if($modereturn){
			if(is_array($result)){
				$this->List = $result;
				return $result;
			}
			return false;
		} else {
			$this->populate($result);
		}
	}
	
	public function loadAll($field = null, $modereturn = false, $limit = null, $order = null){
		
		$class = new $this->_ObjectClass();
		
		$des = $class->TableDescribe();
		
		$select = $class->select();
		$select->setIntegrityCheck(false);
		
		if(isset($field)){
			$select->from($class->getTableName(),$field);
		}else{
			$select->from($class->getTableName());
		}
		
		//depend table
		if(is_array($class->getDependentTables())){
			foreach($class->getDependentTables() as $depTable => $depRules){
				if($depRules['type'] == 'left'){
					$select->joinLeft($depTable, $depRules['value']);
				}
				if($depRules['type'] == 'strict'){
					$select->join($depTable, $depRules['value']);
				}
				if(isset($depRules['where'])){
					$select->where( $depTable .'.'. $depRules['where']['field'] .' = ? ', $depRules['where']['value']);
				}
			}
		}
		
		$this->modifySelect($select);
		
		
		// gestion de limit
		if(isset($limit)){
			if(is_int($limit)){
				if($limit != 0){
					$select->limit($limit);
				}
			}else{
				unset($limit);
			}
		}
		
		if(!isset($limit) && isset($this->limit)){
			if(is_int($this->limit) && $this->limit != 0){
				$select->limit($this->limit);
			}
		}
		
		// gestion de order
		
		if(!isset($order) && isset($this->orderType)){
			$primary = $class->getPrimaryKey();
			if(is_array($primary)){
				$order = array();
				foreach($primary as $key){
					$order[] = $class->getTableName() .'.'. $key.' '.$this->orderType;
				}
				$select->order($order);
				//order by primary list
			}else{
				$select->order(array($class->getTableName() .'.'. $primary.' '.$this->orderType));
			}
		}
		
		//debug querry
		$this->request = $select->assemble();
		
		//execute query
		$rs = $this->fetchAll($select);
		if(empty($rs)){
			return false;
		}
		$result = $rs->toArray();
		$this->TotalResult = count($result);
		if($modereturn){
			if(is_array($result)){
				$this->List = $result;
				return $result;
			}
			return false;
		} else {
			$this->populate($result);
		}
	}
	
	private function checkValuequery($paramField,$class){
		if(!is_array($paramField)){
			return false;
		}
		$des = $class->TableDescribe();
		foreach($paramField as $fieldKey => $fieldVal){
			if(is_array($fieldVal)){
				foreach ($fieldVal as $val){
					switch($des[$fieldKey]){
						case 'int':
							if(!is_numeric($val)){
								return false;
							}
							break;
						case 'string':
							if(!is_string($val)){
								return false;
							}
							break;
						case 'date':
							if(!is_string($val)){
								return false;
							}
							break;
					}
				}
			}else{
				switch($des[$fieldKey]){
					case 'int':
						if(!is_numeric($fieldVal)){
							return false;
						}
						break;
					case 'string':
						if(!is_string($fieldVal)){
							return false;
						}
						break;
					case 'date':
						if(!is_string($fieldVal)){
							return false;
						}
						break;
				}
			}
		}
		return true;
	}
	
	public function populate($values) {
		if(!is_array($values)){
			return false;
		}
		$class = new $this->_ObjectClass();
		foreach($values as $value){
			$class = new $this->_ObjectClass();
			$class->populate($value);
			if($class->check()){
				$this->List[] = $class;
			}
		}
	}
	
	protected function DoQuery ($sql, $params=null) {
		if (isset($params)) {
			$rs = $this->_db->query($sql,$params);
		} else {
			$rs = $this->_db->query($sql);
		}
		$data = $rs->fetchAll();
		return $data;
	}
	
	
}