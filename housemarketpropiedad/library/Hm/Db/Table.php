<?php

/**
 * Controller por defecto
 */
class Hm_Db_Table extends Zend_Db_Table {
	
	public $log;
	
	protected $_dependentTables = null;
	
	protected $_restrictColumns = null;
	
	public $request = null;
	
	function __construct($values = null) {
		if(Zend_Registry::isRegistered('log')){
			$this->log = Zend_Registry::get('log');
		}
		$this->_setAdapter('db');
		if($values != null){
			$this->populate($values);
		}
	}
	
	public function getPrimaryKey(){
		return $this->_primary;
	}
	
	public function addfiltro($columna, $value, $operador = null, $table = null){
		if(is_null($operador)){
			$operador = '=';
		}
		if(is_null($table)){
			$table = $this->_name;
		}
	
		$this->filtros[] = array(
				'table' => $table,
				'columna' => $columna,
				'operador' => $operador,
				'value' => $value,
		);
	}
	/**
	 * Carga el objecto con la primary key
	 * @param unknown_type $primaryKeyVal
	 */
	public function load($primaryKeyVal = null) {
		//recuperacion de la value en el objecto
		$primaryKey = $this->_primary;
		if(is_string($primaryKey)){
			if(is_string($primaryKeyVal)){
				$primaryKeyVal = array($primaryKey => $primaryKeyVal);
			}
			$primaryKey = array($primaryKey);
		}
		$des = $this->TableDescribe();
		foreach ($primaryKey as $primary){
			if(!isset($primaryKeyVal[$primary]) && isset($this->$primary) ){
				$primaryKeyVal[$primary] = $this->$primary;
			}
		}
		
		if(!isset($primaryKeyVal)){
			return false;
		}
		
		if(!$this->checkValuequery($primaryKeyVal)){
			return false;
		}
		
		$select = $this->select();
		$select->setIntegrityCheck(false);
		
		
		if(isset($this->_restrictColumns)){
			$select->from($this->_name,$this->_restrictColumns);
		}else{
			$select->from($this->_name);
		}
		// select by primary
		foreach ($primaryKey as $primary){
			$value = $this->safeval($primaryKeyVal[$primary],$des[$primary]);
			$select->where( $this->_name .'.'. $primary .' = ? ', $value);
		}
		
		//depend table
		if(is_array($this->getDependentTables())){
			foreach($this->getDependentTables() as $depTable => $depRules){
				if($depRules['type'] == 'left'){
					$select->joinLeft($depTable, $depRules['value']);
				}
				if($depRules['type'] == 'strict'){
					$select->join($depTable, $depRules['value']);
				}
				if(isset($depRules['where'])){
					$select->where( $depTable .'.'. $depRules['where']['field'] .' = ? ', $depRules['where']['value']);
				}
			}
		}
		
		//debug querry
		$this->request = $select->assemble();
		
		$rs = $this->fetchAll($select);
		if(empty($rs)){
			return false;
		}
		$result = $rs->toArray();
		if(is_array($result) && isset($result[0])){
			$this->populate($result[0]);
		}else{
			unset($this->$primary);
		}
	}
	
	/**
	 * carga el objecto con el resltado de una seleccion
	 * @param unknown_type $paramField
	 */
	public function search($paramField = null, $modereturn = false) {
		if(!isset($paramField) && !is_array($paramField)){
			return false;
		}
		$des = $this->TableDescribe();
		
		foreach($paramField as $fieldKey => $fieldVal){
			$objval = $this->{$fieldKey};
			if(!isset($fieldVal) && isset($objval)){
				$paramField[$fieldKey] = $objval;
			}
		}
	
		if(!$this->checkValuequery($paramField)){
			return false;
		}
		$select = $this->select();
		$select->setIntegrityCheck(false);
		
		if(isset($this->_restrictColumns)){
			$select->from($this->_name,$this->_restrictColumns);
		}else{
			$select->from($this->_name);
		}
		
		// select by field
		foreach($paramField as $fieldKey => $fieldVal){
			$fieldVal = $this->safeval($fieldVal,$des[$fieldKey]);
			$select->where( $this->_name .'.'. $fieldKey .' = ? ', $fieldVal);
		}
		
		//depend table
		if(is_array($this->getDependentTables())){
			foreach($this->getDependentTables() as $depTable => $depRules){
				if($depRules['type'] == 'left'){
					$select->joinLeft($depTable, $depRules['value']);
				}
				if($depRules['type'] == 'strict'){
					$select->join($depTable, $depRules['value']);
				}
				if(isset($depRules['where'])){
					$select->where( $depTable .'.'. $depRules['where']['field'] .' = ? ', $depRules['where']['value']);
				}
			}
		}
		
		// filtro
		if(!empty($this->filtros)){
			foreach($this->filtros as $filtro){
				$where = '';
				if(isset($filtro['table'])){
					$where .= $filtro['table'] .'.';
				}
				if($filtro['value'] == "NOT NULL"){
					$where .= $filtro['columna'] .' IS NOT NULL ';
					$select->where( $where);
				}elseif ($filtro['value'] == "NULL"){
					$where .= $filtro['columna'] .' IS NULL' ;
					$select->where( $where);
				}else {
					if(is_array($filtro['value'])){
						$wherelist = array();
						foreach($filtro['value'] as $val){
							$wherelist[] = $where . $filtro['columna'] .' '. $filtro['operador'] .' \''. $val .'\' ';
						}
						$where = implode(' OR ', $wherelist);
						$select->where( $where);
					}else{
						$where .= $filtro['columna'] .' '. $filtro['operador'] .' ?';
						$select->where( $where, $filtro['value']);
					}
				}
			}
		}
		
		//debug querry
		$this->request = $select->assemble();
		
		$rs = $this->fetchAll($select);
		if(empty($rs)){
			return false;
		}
		
		$result = $rs->toArray();
		if($modereturn){
			if(is_array($result) && isset($result[0])){
				return $result[0];
				
			}
			return false;
		} else {
			if(isset($result[0])){
				$this->populate($result[0]);
			}
		}
	}
	
	private function checkValuequery($paramField){
		if(!is_array($paramField)){
			return false;
		}
		$des = $this->TableDescribe();
		foreach($paramField as $fieldKey => $fieldVal){
			switch($des[$fieldKey]){
				case 'int':
					if(!is_numeric($fieldVal)){
						return false;
					}
					break;
				case 'string':
					if(!is_string($fieldVal)){
						return false;
					}
					break;
				case 'date':
					// format ??
					break;
			}
		}
		return true;
	}
	
	public function populate($values) {
		if(!is_array($values)){
			return false;
		}
		foreach($this->TableDescribe() as $key => $type){
			if(isset($values[$key])){
				//transform revert date
				$this->{$key} = $values[$key];
			}
		}
	}
	
	public function save() {
		$primaryKey = $this->_primary;
		if(is_string($primaryKey)){
			$primaryKey = array($primaryKey);
		}
		$primaryKeyVal = array();
		foreach ($primaryKey as $primary){
			if(!isset($primaryKeyVal[$primary]) && isset($this->$primary) ){
				$primaryKeyVal[$primary] = $this->$primary;
			}
		}
		
		$updaterequest = false;
		// verifimos que tenemos todas la primary key para update
		if(count($primaryKeyVal) == count($primaryKey)){
			$updaterequest = true;
		}
		// charging data
		$data = array();
		
		foreach($this->TableDescribe() as $key => $type){
			$data[$key] = $this->safeval($this->{$key},$type);
		}
		try{
			if($updaterequest){
				$params = array();
				foreach ($primaryKeyVal as $key => $value){
					$params[] = $value;
					$primaryListWhere [] = $key .' = ?';
				}
				
				$where = $this->getAdapter()->quoteInto(implode('and',$primaryListWhere), $params);
				$this->update($data,$where);
			}else{
				$this->insert($data);
				if(count($primaryKey) == 1){
					$this->$primary = $this->getAdapter()->lastInsertId();
				}
			}
		}catch (Exception $e){
			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
		}
	}
	
	public function delete() {
		$primaryKey = $this->_primary;
		if(is_string($primaryKey)){
			$primaryKey = array($primaryKey);
		}
		$des = $this->TableDescribe();
		$primaryKeyVal = array();
		foreach ($primaryKey as $primary){
			if(!isset($primaryKeyVal[$primary]) && isset($this->$primary) ){
				$primaryKeyVal[$primary] = $this->$primary;
			}
		}
	
		$updaterequest = false;
		// verifimos que tenemos todas la primary key para update
		if(count($primaryKeyVal) == count($primaryKey)){
			$updaterequest = true;
		}
		$params = array();
		foreach ($primaryKeyVal as $key => $value){
			$params[] = $this->safeval($value,$des[$key]);
			$primaryListWhere [] = $key .' = ?';
		}

		$where = $this->getAdapter()->quoteInto(implode('and',$primaryListWhere), $params);
		parent::delete($where);
	
	}
	
	protected function DoQuery ($sql, $params=null) {
		if (isset($params)) {
			$rs = $this->_db->query($sql,$params);
		} else {
			$rs = $this->_db->query($sql);
		}
		$data = $rs->fetchAll();
		return $data;
	}
	
	public function getTableName(){
		return $this->_name;
	}
	
	public function getDependentTables(){
		return $this->_dependentTables;
	}
	
	public function check(){
		return true;
	}
	
	public function safeval($value,$type){
		if($type == 'date'){
			if($value == null){
				return null;
			}
			return date( 'Y-m-d H:i:s', $value );
		}
		if($type == 'string'){
			return addslashes($value);
		}
		if($type == 'int'){
			//TODO test
			return $value;
		}
		return $value;
	}
}