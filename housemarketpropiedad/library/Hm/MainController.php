<?php

/**
 * Controller por defecto
 */
class Hm_MainController extends Zend_Controller_Action {

	protected $translator;
	protected $cfg;
	protected $db;
	protected $signed;
	protected $currentActionNav;
	protected $fbparams;
	protected $app;
	protected $fbDgt;
	protected $uri;
	
    /**
     * Inicializacion del controller
     */
    public function init() {
    	if(Zend_Registry::isRegistered('Zend_Translate')){
    		$this->translator=Zend_Registry::get('Zend_Translate');
    	}
    	if(Zend_Registry::isRegistered('config')){
    		$this->cfg = Zend_Registry::get('config');
    	}
    	if(Zend_Registry::isRegistered('db')){
    		$this->db = Zend_Registry::get('db');
    	}
    	if(Zend_Registry::isRegistered('Facebook_Signed')){
    		$this->Facebook_Signed = Zend_Registry::get('Facebook_Signed');
    		$this->view->signedHash = $this->Facebook_Signed->get('code');
    	}
    	if(Zend_Registry::isRegistered('language')){
    		$this->language = Zend_Registry::get('language');
    	}
    	if(Zend_Registry::isRegistered('Facebook_App')){
    		$this->app = Zend_Registry::get('Facebook_App');
    	}

    	https://connect.facebook.net/en_US/all.js
    	
    	switch(Zend_Registry::get('language')){
    		case 'es':
    			$this->view->headScript()->appendFile('https://connect.facebook.net/es_MX/all.js');
    			break;
    		case 'en':
    			$this->view->headScript()->appendFile('https://connect.facebook.net/en_US/all.js');
    			break;
    	}
    	
    	//Message
    	
    	$message = Zend_Registry::get('message');
    	$this->view->message = $message->Deliver();
    	
    	
    	//header fb
    	$this->view->headerMeta = new Hm_View_HeaderMeta();
    	//$this->view->headerMeta->setProperty('fb:admins',$this->cfg['facebook_admin']);
    	$this->view->headerMeta->setProperty('fb:app_id',$this->cfg['facebook_appid']);
    	$this->view->headerMeta->setProperty('og:type','website');
    	$this->view->headerMeta->setProperty('og:url', $this->cfg['fb_app_url']);
    	$this->view->headerMeta->setProperty('og:title','Propiedad Housemarket');
    	$this->view->headerMeta->setProperty('og:image','https://photos-h.ak.fbcdn.net/photos-ak-snc1/v27562/245/109002199131497/app_1_109002199131497_7294.gif');
    	$this->view->headerMeta->setProperty('og:site_name','Housemarket Tab');
    	
    	//Meta clasic
    	$this->view->headerMeta->setName('title','House Market');
    	$this->view->headerMeta->setName('description',$this->view->appDescription);
    	
    	// default meta Og
    	
    	
    	$this->view->headerMeta->setProperty('og:country-name','Nicaragua');
    	$desc = $this->translator->translate("APPLICATION_DESCRIPTION");
    	$this->view->headerMeta->setProperty('og:description',$desc);
    	
    	// default link
    	//$this->view->headerMeta->setLink('image_src','https://app.hmapp.com/img/logohm.png');
    	//$this->view->headerMeta->setlink('target_url',$this->cfg['local_app_url']);
    	
    	$this->view->headerScript = new Hm_View_HeaderScript();
    	$facebook_env = "var env = {
    		canvas_url : '". $this->cfg['local_app_url'] ."',
    		facebook_url : '". $this->cfg['fb_app_url'] ."'
    		};
    	";
    	$this->view->headerScript->setScript($facebook_env,'facebook_env');
    	// include facebook connect
    	$facebook_connect = "FB.init({
    		appId: '". $this->cfg['facebook_appid'] ."',
    		status : true, // check login status
    		cookie : true, // enable cookies to allow the server to access the session
    		xfbml  : true,  // parse XFBML
    		oauth  : true
    	});";
    	$this->view->headerScript->setScript($facebook_connect,'facebook_connect');
    	
    	
    	$this->view->headLink()->appendStylesheet('/css/main.css');
    	$this->view->headScript()->appendFile('/js/jquery/jquery-1.3.2.min.js');
    	$this->view->headScript()->appendFile('/js/main.js');
    	$this->view->facebook_appid = $this->cfg['facebook_appid'];
    	$this->view->facebook_perms = $this->cfg['facebook_perms'];
    	$this->view->local_app_url = $this->cfg['local_app_url'];
    	$this->view->fb_app_url = $this->cfg['fb_app_url'];
    	$this->view->img_src_url = $this->cfg['img_src_url'];
    	$this->view->bodyClass = 'body';
    	
    	$this->view->footerScript = new Hm_View_FooterScript();
    	
    	$this->view->footerScript->setScript("FB.Canvas.setAutoGrow();\n FB.Canvas.setSize({ width: 1030, height: 925 });",'text/javascript');
    	//cut URI
    	if(strpos($this->_request->getRequestUri(),'?') !== false){
    		list($uri, $query)= explode ( '?' , $this->_request->getRequestUri());
    	}else{
    		$uri = $this->_request->getRequestUri();
    	}
    	$this->uri = explode ( '/' , $uri );
    	
    }

    protected function loadJQueryPlugin($name = null) {
    	$this->view->headScript()->appendFile('/js/jquery/jquery-1.3.2.min.js');
    	if(is_array($name)){
    		$names = $name;
    		foreach ($names as $name){
    			$this->loadJQuerySinglePlugin($name);
    		}
    	}
    	
    	if(is_string($name)){
    		$this->loadJQuerySinglePlugin($name);
    	}
    	
    }
    
    private function loadJQuerySinglePlugin($name = null){
    	if($name != null){
    		// Special plugin with external include
    		switch ($name){
    			case 'gmap':
    				//&amp;sensor=false
    				
    				//$filename = 'https://maps.google.com/maps?sensor=false&file=api&v=3&key='. $this->cfg['google_api_key'];
    				$filename = 'https://maps.googleapis.com/maps/api/js?key='. $this->cfg['google_api_key'] . '&sensor=false' ;
    				$this->view->headScript()->appendFile($filename, 'text/javascript');
    				$filename = '/js/jquery/plugins/'. $name .'/'. $name .'.markerclusterer.js';
    				if(file_exists ( PUBLIC_HTML_PATH . $filename )){
    					$this->view->headScript()->appendFile($filename);
    				}
    				break;
    		}
    		
    		// core del plugin
    		$filename = '/js/jquery/plugins/'. $name .'/jquery.'. $name .'.js';
    		if(file_exists ( PUBLIC_HTML_PATH . $filename )){
    			$this->view->headScript()->appendFile($filename);
    		}
    		
    		// css del plugin
    		$filename = '/js/jquery/plugins/'. $name .'/css/'. $name .'.css';
    		if(file_exists ( PUBLIC_HTML_PATH . $filename )){
    			$this->view->headLink()->appendStylesheet($filename);
    		}
    		// conf del plugin
    		$filename = '/js/'. $name .'-conf.js';
    		if(file_exists ( PUBLIC_HTML_PATH . $filename )){
    			$this->view->headScript()->appendFile($filename);
    		}
    	}
    }
    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
    	// verify the validity of user
    	$signed_request = Zend_Registry::get('Facebook_Signed');
    	$params = $this->_request->getParams();
    	$params['signed_request'] = $signed_request;
    	$params['old_param'] = $params;
    	if(!$signed_request->check($params)){
    		$params['controller'] = 'oauth';
    		$params['module'] = 'default';
    		$params['action'] = 'authorize';
    		$this->_forward($params['action'], $params['controller'], $params['module'], $params);
    	}
    }

    /**
     * 
     * fonction de retour de label de navigation courant
     */
    public function getCurrentLabelnav() {
    	return '';
    }
    
    /**
    *
    * fonction de retour de label de navigation courant
    */
    public function getCurrentActionNav() {
    	return $this->currentActionNav;
    }
    
    public function setOgVariable($variables,$value = null) {
    	if(is_string($variables) && !is_null($value)){
    		$variables_array = array($variables => $value);
    		$variables = $variables_array;
    	}
    	
    	if(!is_array($variables)){
    		return null;
    	}
    	foreach ($variables as $key => $value){
    		$this->view->headMeta()->setProperty('og:'. $key,$value);
    	}
    	
    }

    protected function pager(&$List,$itemPerPage = 10){
    	$pagerID = 1;
    	if(isset($_GET['pager'])){
    		$pagerID = $_GET['pager'];
    	}
    	$nb_list = count($List);
    	
    	$List = array_slice($List, (($pagerID - 1) * $itemPerPage), $itemPerPage);
    
    	if(strrpos($_SERVER['REQUEST_URI'], '&pager=')){
    		$url = strstr($_SERVER['REQUEST_URI'], '&pager=', true);
    	}else{
    		$url = $_SERVER['REQUEST_URI'];
    	}
    	
    	$pagerSize = round(($nb_list / $itemPerPage)); //(attention entier)

    	$this->view->pager = new Hm_View_Pager($url,$pagerID,$pagerSize);
    }
    
    protected function getInstallUrl($redirectUri = null){
    	if(!isset($redirectUri)){
    		$redirectUri = $this->cfg['local_app_url'] .'register';
    	}
    	$scope = array(
    			'manage_pages',
    			'user_about_me',
    			'publish_actions',
    			'email',
    			'user_birthday',
    	);
    	$scope_string = implode(',',$scope);
    	
    	$url = 'https://www.facebook.com/dialog/oauth/';
    	$url .= '?client_id='. $this->cfg['facebook_appid'] ;
    	$url .= '&redirect_uri='. urlencode($redirectUri);
    	$url .= '&scope='. $scope_string;
    	return $url;
    }
    
    protected function isAdmin($uid = null,$pageId = null){
    	if(!isset($uid) || $uid == ''){
    		return false;
    	}
    	if(!isset($pageId)){
    		return false;
    	}
    	$url = '/'. $uid .'/accounts';
    	if($url == '//accounts'){
    		return false;
    	}
    	$page_accounts = $this->app->api($url);
    	foreach($page_accounts['data'] as $page){
    		if($page['id'] == $pageId && isset($page['access_token'])){
    			return true;
    		}
    	}
    	return false;
    }
}

?>