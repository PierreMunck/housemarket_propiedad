<?php 

class Hm_Form_Filter extends Hm_Form{
	
	private $signed_request = null;
	
	protected $formDesc = array(
			'Protype' => array(
					'type' => 'Select',
					'Label' => 'Tipo de propiedad',
					'required' => true,
					'#option' => array('todos' => 'todos'),
			),
			'ProPrecioMin' => array(
					'type' => 'Select',
					'Label' => 'Tipo de propiedad',
					'required' => true,
					'#option' => array('1' => '1'),
			),
			'ProPrecioMax' => array(
					'type' => 'Select',
					'Label' => 'Tipo de propiedad',
					'required' => true,
					'#option' => array('1' => '1'),
			),
			'ProAreaMin' => array(
					'type' => 'Select',
					'Label' => 'Tipo de propiedad',
					'required' => true,
					'#option' => array('1' => '1'),
			),
			'ProAreaMax' => array(
					'type' => 'Select',
					'Label' => 'Tipo de propiedad',
					'required' => true,
					'#option' => array('1' => '1'),
			),
			'ProCuarto' => array(
					'type' => 'Select',
					'Label' => 'Tipo de propiedad',
					'required' => true,
					'#option' => array('1' => '1'),
			),
			'ProBano' => array(
					'type' => 'Select',
					'Label' => 'Tipo de propiedad',
					'required' => true,
					'#option' => array('1' => '1'),
			),
			'submitName' => 'search',
			'submitLabel'=> 'Search',
			'#action' => '/register/post',
		);
	
	public function __construct($formState = null , $params = null){
		parent::__construct('form-filter',$formState,$params);
	}
	
	public function validate(){
		return true;
	}
	
	public function submit(){
		
	}
}

?>