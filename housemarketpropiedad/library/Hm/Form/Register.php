<?php 

class Hm_Form_Register extends Hm_Form{
	
	private $signed_request = null;
	
	protected $formDesc = array(
			'firstname' => array(
					'type' => 'Text',
					'required' => true,
					'Label' => 'firstname',
					),
			'lastname' => array(
					'type' => 'Text',
					'required' => true,
					'Label' => 'lastname',
			),
			'EMail' => array(
					'type' => 'Email',
					'required' => true,
					'Label' => 'Email',
			),
			'Pais' => array(
					'type' => 'Select',
					'Label' => 'Pais',
					'required' => true,
					'#option' => array(),
			),
			'PageId' => array(
					'type' => 'Select',
					'Label' => 'Page',
					'required' => true,
					'#option' => array(),
			),
			'TelefonoCliente' => array(
					'type' => 'Text',
					'Label' => 'Telefono personal',
			),
			'NombreEmpresa' => array(
					'type' => 'Text',
					'Label' => 'Nombre de la empresa',
			),
			'TelefonoEmpresa' => array(
					'type' => 'Text',
					'Label' => 'Telefono de la empresa',
			),
			'YouVideo' => array(
					'type' => 'Text',
					'Label' => 'video de presentacion',
			),
			'Twitter' => array(
					'type' => 'Text',
					'Label' => 'Cuenta Twitter',
			),
			'Webpage' => array(
					'type' => 'Text',
					'Label' => 'Pagina Web',
			),
			'condition' => array(
					'type' => 'Radio',
					'Label' => 'Condicion',
					'required' => true,
					'#option' => array(
							1 => 'Si',
							0 => 'No',
					),
			),
			'submitName' => 'val',
			'submitLabel'=> 'Register',
			'#action' => '/register/post',
		);
	
	public function __construct($signed, $formState = null , $params = null){
		$this->signed_request = $signed;
		parent::__construct('form-register',$formState,$params);
		
		if(isset($this->formState['values']['PageId'])){
			$page_id = $this->formState['values']['PageId'];
			$page = new Hm_Cli_Page();
			$page->search(array('pageid' => $page_id));
		}
		// Carga de los paises
		$Pais = new Hm_Form_Element_Pais();
		$this->formDesc['Pais']['#option'] = $Pais->getOptions();
		
		// default country
		$UserPais = $this->signed_request->getLocation();
		if(!isset($this->formState['values']['Pais']) && isset($UserPais['country'])){
			$this->formState['values']['Pais'] = $Pais->getcode($UserPais['country']);
		}
		
		// Carga user page
		$UserPaginas = $this->signed_request->getPaginas();
		if(isset($UserPaginas)){
			$this->formDesc['PageId']['#option'] = $UserPaginas;
		}
		
		// default first name
		$firstname = $this->signed_request->get('first_name');		
		if(!isset($this->formState['values']['firstname']) && isset($firstname)){
			$this->formState['values']['firstname'] = $firstname;
		}
		
		// default last name
		$lastname = $this->signed_request->get('last_name');
		if(!isset($this->formState['values']['lastname']) && isset($lastname)){
			$this->formState['values']['lastname'] = $lastname;
		}
		
		// default email
		if(isset($page) && isset($page->email)){
			$email = $page->email;
		}else{
			$email = $this->signed_request->get('email');
		}
		if(!isset($this->formState['values']['EMail']) && isset($email)){
			$this->formState['values']['EMail'] = $email;
		}
		
		//default Nombre de empresa
		if(isset($page) && isset($page->nombreempresa)){
			$work = array();
			$work[] = array('employer' => array('name' => $page->nombreempresa));
		}else{
			$work = $this->signed_request->get('work');
		}
		if(!isset($this->formState['values']['NombreEmpresa']) && isset($work)){
			if(!empty($work) && isset($work[0]['employer'])){
				$this->formState['values']['NombreEmpresa'] = $work[0]['employer']['name'];
			}
		}
		
		//default Telefono personal
		if(!isset($this->formState['values']['TelefonoCliente']) && isset($page) && isset($page->telefono)){
			$this->formState['values']['TelefonoCliente'] = $page->telefono;
		}

		//default Telefono de la empresa
		if(!isset($this->formState['values']['TelefonoEmpresa']) && isset($page) && isset($page->telefonoempresa)){
			$this->formState['values']['TelefonoEmpresa'] = $page->telefonoempresa;
		}
		
		//default video de presentacion
		if(!isset($this->formState['values']['YouVideo']) && isset($page) && isset($page->youvideo)){
			$this->formState['values']['YouVideo'] = $page->youvideo;
		}
	
		//default Cuenta Twitter
		if(!isset($this->formState['values']['Twitter']) && isset($page) && isset($page->twitter)){
			$this->formState['values']['Twitter'] = $page->twitter;
		}
		
		//default Pagina Web
		if(!isset($this->formState['values']['Webpage']) && isset($page) && isset($page->webpage)){
			$this->formState['values']['Webpage'] = $page->webpage;
		}
	}
	
	public function validate(){
		$error = false;
		if(isset($this->formState['values'])){
			if(!isset($this->formState['values']['condition'])){
				$error = true;
				$this->formDesc['condition']['error'] = true;
				$this->formDesc['condition']['errorMsg'] = 'Accept condition';
			}else{
				if($this->formState['values']['condition'] != true){
					$error = true;
					$this->formDesc['condition']['error'] = true;
					$this->formDesc['condition']['errorMsg'] = 'Accept condition';
				}
			}
		}
		if(!$error && parent::validate()){
			return true;
		}
		return false;
	}
	
	public function submit(){
		$signed = Zend_Registry::get('Facebook_Signed');
		
		if(isset($signed)){
			$uid = $signed->get('user_id');
			if(isset($uid)){
				$this->formState['values']['Uid'] = $uid;
			}
		}
		if(isset($this->formState['values'])){
			if(isset($this->formState['values']['firstname']) && isset($this->formState['values']['lastname'])){
				$this->formState['values']['NombreCliente'] = $this->formState['values']['firstname'] ." ".$this->formState['values']['lastname'];
			}
		}
		// table register
		$TableRegister = new Hm_Cli_RegisterTab($this->formState['values']);
		$TableRegister->save();
		
		//page
		$data = array();
		$data['pageid'] = $this->formState['values']['PageId'];
		$data['nombre'] = $this->formState['values']['NombreCliente'];
		$data['email'] = $this->formState['values']['EMail'];
		$data['youvideo'] = $this->formState['values']['YouVideo'];
		$data['twitter'] = $this->formState['values']['Twitter'];
		$data['telefono'] = $this->formState['values']['TelefonoCliente'];
		$data['webpage'] = $this->formState['values']['Webpage'];
		$data['telefonoempresa'] = $this->formState['values']['TelefonoEmpresa'];
		$data['nombreempresa'] = $this->formState['values']['NombreEmpresa'];
		
		$page = new Hm_Cli_Page($data);
		$page->save();
		
		// Page Service Status
		$data = array();
		$data['pageId'] = $this->formState['values']['PageId'];
		$data['pageName'] = $this->formState['values']['NombreEmpresa'];
		$data['serviceType'] = 1;
		$data['serviceStatus'] = 0;
		$data['serviceStartDate'] = null;
		$data['serviceExpirationDate'] = null;
		$data['hmNotification'] = 0;
		$data['clientNotification'] = 0;
		$data['requestDate'] = time();
		$pageServicestatus = new Hm_Cli_PageServiceStatus($data);
		$pageServicestatus->save();
		
		// Fb Page
		if(isset($uid)){
			$data = array();
			$data['uid'] = $uid;
			$data['pageid'] = $this->formState['values']['PageId'];
			$data['dateIngreso'] = time();
			$fb_page = new Hm_Fb_Page($data);
			$fb_page->save();
		}
		
		$app = Zend_Registry::get('Facebook_App');
		$cfg = Zend_Registry::get('config');
		
		// push buton on page
		$app->setPageAccessToken($uid,$this->formState['values']['PageId']);
		$params = array('app_id' => $cfg['facebook_appid']);
		$graph = '/'. $this->formState['values']['PageId'] .'/tabs';
		$app->api($graph,'POST',$params);
		$app->setUserAccessToken();
		
		$message = new Hm_Facebook_Message('join housemarket app');
		$message->picture = $cfg['local_app_url'] .'/img/house_azul.png';
		$message->link = $cfg['local_app_url'] .'/application';
		$signed->pushwall($message);
		
		//mandar mail del formulario
		
		//mandar mail a nosotros
		
		
		// permision to write page wall
		/*try {
			$app->setPageAccessToken($this->formState['values']['PageId']);
			$graph = '/'. $this->formState['values']['PageId'] .'/feed';
			$app->setUserAccessToken();
			$app->api($graph, 'post', $message->toarray());
		} catch(FacebookApiException $e){

		}*/
		
		
		return $TableRegister->CodigoRegister;
	}
}

?>