<?php 
class Hm_Form {
	
	protected $formId = null;
	protected $formState = null;
	protected $formDesc = null;
	protected $formAction = false;
	protected $renderSubmit = false;
	protected $params = null;
	protected $submited = false;
	
	protected $CampoEspecial = array(
			'Label',
			'submitName',
			'submitLabel',
			'MessagePosition',
		);
	
	public function __construct($idForm, $formState,$params){
		$this->formId = $idForm;
		$this->formState['values'] = $formState;
		if(isset($this->formDesc['#action'])){
			$this->formAction = $this->formDesc['#action'];
		}
	}
	
	public function __toString() {
		$token_value = $this->formId;
		$output = "\n";
		$output .= '<form class="'. $this->formId  .'" action="'. $this->formAction .'" method="post">';
		if(isset($this->formDesc['Label'])){
			$output .= '<span class="'. $this->formId  .'-title">'. $this->formDesc['Label'] .'</span>';
		}
		if(isset($this->formDesc['Message'])){
			if(isset($this->formDesc['MessagePosition']) && $this->formDesc['MessagePosition'] == 'bottom'){
				// push on bottom
			}else{
				$output .= '<span class="'. $this->formId  .'-message">'. $this->formDesc['Message'] .'</span>';
			}
		}
		foreach($this->formDesc as $name => $desc){
			if(isset($desc['type'])){
				$function = 'render'.$desc['type'];
				if(method_exists($this,$function)){
					$output .= $this->$function($name,$desc);
				}
			}
			if(isset($desc['type']) && $desc['type'] == 'Hidden'){
				$token_value .= $this->getValue($name);
			}
		}
		if($this->formAction == ''){
			$value = md5 ( $token_value );
			$output .= '<input type="hidden" name="'. $this->formId  .'" value="'. $value .'"/>';
		}
		if(!$this->renderSubmit){
			$output .= $this->renderSubmit($this->formDesc['submitName'],array('Label' => $this->formDesc['submitLabel']));
		}
		if(isset($this->formDesc['Message']) && isset($this->formDesc['MessagePosition']) && $this->formDesc['MessagePosition'] == 'bottom'){
			$output .= '<span class="'. $this->formId  .'-message">'. $this->formDesc['Message'] .'</span>';
		}
		$output .= '</form>';
		$output .= "\n";
		return $output;
	}
	
	public function validate(){
		$error = false;
		
		if($this->formAction == ''){
			$token_value = $this->formId;
			foreach($this->formDesc as $name => $desc){
				if(isset($desc['type']) && $desc['type'] == 'Hidden'){
					$token_value .= $this->getValue($name);
				}
			}
			if(!isset($this->formState['values']) || !isset($this->formState['values'][$this->formId])){
				$this->formDesc[$this->formId]['errortoken'] = true;
				$error = true;
			}
			$md5 = md5 ( $token_value );
			if(isset($this->formState['values'][$this->formId]) && $this->formState['values'][$this->formId] != $md5){
				$error = true;
				$this->formDesc[$this->formId]['errortoken'] = true;
			}
		}
		
		foreach($this->formDesc as $name => $desc){
			if(in_array($name,$this->CampoEspecial)){
				continue;
			}
			if(isset($desc['type']) &&$desc['type'] == 'Email'){
				if(isset($this->formState['values']) && isset($this->formState['values'][$name]) && !$this->checkMail($this->formState['values'][$name])){
					$this->formDesc[$name]['error'] = true;
					$this->formDesc['Message'] = " ". $name ." must be a valid email";
					$error = true;
				}
			}
			if(isset($desc['required']) && $desc['required'] == true){
				if(!isset($this->formState['values'])){
					$this->formDesc[$name]['error'] = true;
					$this->formDesc['Message'] = $name ." is required";
					$error = true;
				}
				if(!isset($this->formState['values'][$name]) || $this->formState['values'][$name] == ''){
					$this->formDesc[$name]['error'] = true;
					$this->formDesc['Message'] = $name ." is required";
					$error = true;
				}
			}
		}
		if(!$error){
			return true;
		}
		return false;
	}
	
	public function run(){
		if($this->submited){
			if($this->validate()){
				$this->submit();
				$submit = true;
			}
		}
	}
	
	public function submit(){
		$this->formState['submited'] = true;
	}
	
	public function getValue($name){
		$value = null;
		if(isset($this->formState['values']) && isset($this->formState['values'][$name])){
			$value = $this->formState['values'][$name];
		}else if(isset($desc['value'])){
			$value = $desc['value'];
		}
		return $value;
	}
	
	public function setDescription($description){
		$this->formDesc = $description;
	}
	
	public function setAction($description){
		$this->formAction = $formAction;
	}
	
	private function checkMail($email){
		if(preg_match("/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/",
				$email)){
			/*list($username,$domain)=split('@',$email);
			if(!checkdnsrr($domain,'MX')) {
				return false;
			}*/
			return true;
		}
		return false;
	}
	
	protected function renderEmail($name, $desc){
		return $this->renderText($name, $desc);
	}
	
	protected function renderText($name, $desc){
		$spanclass = '';
		$inputclass = '';
		$class = '';
		$value = '';
		$required = '';
		if(isset($desc['class'])){
			$class = $desc['class'];
			$spanclass = 'span-'.$desc['class'];
			$inputclass = 'input-'.$desc['class'];
		}
		
		if(isset($desc['error']) && $desc['error'] == true){
			$class .= ' error';
		}
		
		if(isset($desc['required']) && $desc['required'] == true){
			$required = " *";
		}
		
		if(isset($desc['Labelinner']) && $desc['Labelinner'] == true){
			$value = $desc['Label'] . $required;
		}
		
		if(isset($this->formState['values']) && isset($this->formState['values'][$name])){
			$value = $this->formState['values'][$name];
		}
		
		$output = '<div class="text '. $class .'">';
		if(!isset($desc['Labelinner']) || (isset($desc['Labelinner']) && $desc['Labelinner'] == false)){
			$output .= '<span class="'. $spanclass .'">'. $desc['Label'] . $required .':</span>';
		}
		$output .= '<input class="input-text '. $inputclass .'" type="text" name="'. $name .'" ';
		if(isset($desc['Labelinner']) && $desc['Labelinner'] == true){
			$output .= 'onfocus="if(this.value==\''. $value .'\') { this.value = \'\';} "';
		}
		$output .= 'value="'. $value .'"/>';
		$output .= '</div>';
		return $output;
	}
	
	protected function renderTextArea($name, $desc){
		$spanclass = '';
		$inputclass = '';
		$class = '';
		$value = '';
		$required = '';
		if(isset($desc['class'])){
			$class = $desc['class'];
			$spanclass = 'span-'.$desc['class'];
			$inputclass = 'input-'.$desc['class'];
		}
		
		if(isset($desc['error']) && $desc['error'] == true){
			$class .= ' error';
		}
		
		if(isset($desc['required']) && $desc['required'] == true){
			$required = " *";
		}
		
		if(isset($desc['Labelinner']) && $desc['Labelinner'] == true){
			$value = $desc['Label'] . $required;
		}
		
		if(isset($this->formState['values']) && isset($this->formState['values'][$name])){
			$value = $this->formState['values'][$name];
		}
		
		$output = '<div class="text '. $class .'">';
		if(!isset($desc['Labelinner']) || (isset($desc['Labelinner']) && $desc['Labelinner'] == false)){
			$output .= '<span class="'. $spanclass .'">'. $desc['Label'] . $required .':</span>';
		}
		$output .= '<textarea class="input-textarea '. $inputclass .'" type="textarea" name="'. $name .'" ';
		if(isset($desc['Labelinner']) && $desc['Labelinner'] == true){
			$output .= 'onfocus="if(this.value==\''. $value .'\') { this.value = \'\';} "';
		}
		$output .= '>';
		$output .=  $value;
		$output .= '</textarea>';
		$output .= '</div>';
		return $output;
	}
	
	protected function renderRadio($name, $desc){
		$spanclass = '';
		$inputclass = '';
		$class = '';
		$value = '';
		$required = '';
		if(isset($desc['class'])){
			$class = $desc['class'];
			$spanclass = 'span-'.$desc['class'];
			$inputclass = 'input-'.$desc['class'];
		}
		
		if(isset($desc['error']) && $desc['error'] == true){
			$class .= ' error';
		}
		
		if(isset($desc['required']) && $desc['required'] == true){
			$required = " *";
		}
		
		$valueState = null;
		if(isset($this->formState['values']) && isset($this->formState['values'][$name])){
			$valueState = $this->formState['values'][$name];
		}
		
		$output = '<div class="radio '. $class .'">';
		$output .= '<span class="'. $spanclass .'">'. $desc['Label'] . $required .':</span>';
		
		foreach($desc['#option'] as $key => $value){
			$selcted = '';
			if($valueState == $key){
				$selcted = 'selected';
			}
			$output .= '<input type="radio" class="input-radio '. $inputclass .'" name="'. $name .'" value="'. $key .'" '. $selcted .' />';
			$output .= '<label>'. $value .'</label>';
		}
		
		$output .= '</div>';
		return $output;
	}
	
	protected function renderSelect($name, $desc){
		$spanclass = '';
		$inputclass = '';
		$class = '';
		$value = '';
		$required = '';
		if(isset($desc['class'])){
			$class = $desc['class'];
			$spanclass = 'span-'.$desc['class'];
			$inputclass = 'input-'.$desc['class'];
		}
	
		if(isset($desc['error']) && $desc['error'] == true){
			$class .= ' error';
		}
	
		if(isset($desc['required']) && $desc['required'] == true){
			$required = " *";
		}
	
		$valueState = null;
		if(isset($this->formState['values']) && isset($this->formState['values'][$name])){
			$valueState = $this->formState['values'][$name];
		}
	
		$output = '<div class="select '. $class .'">';
		$output .= '<span class="'. $spanclass .'">'. $desc['Label'] . $required .':</span>';
		$output .= '<select class="input-radio '. $inputclass .'" name="'. $name .'" >';
		if(isset($desc['required']) && $desc['required'] == false){
			'<option value="">choose one</option>'; 
		}
		foreach($desc['#option'] as $key => $value){
			$selcted = '';
			if($valueState == $key){
				$selcted = 'selected';
			}
			$output .= '<option value="'. $key .'" '. $selcted .' >';
			$output .= $value;
			$output .= '</option>';
		}
		$output .= '</select>';
		$output .= '</div>';
		return $output;
	}
	
	protected function renderSubmit($name, $desc){
		$output = '';
		if(!$this->renderSubmit){
			$spanclass = '';
			$inputclass = '';
			$class = '';
			if(isset($desc['class'])){
				$class = $desc['class'];
				$spanclass = 'span-'.$desc['class'];
				$inputclass = 'input-'.$desc['class'];
			}
			
			if(isset($desc['error']) && $desc['error'] == true){
				$class .= ' error';
			}
			
			$output .= '<div class="submit '. $class .'">';
			$output .= '<input type="submit" class="input-submit '. $inputclass .'" name="'. $name .'" value="'. $desc['Label'] .'"/>';
			$output .= '</div>';
			$this->renderSubmit = true;
		}
		return $output;
	}
	
	protected function renderHidden($name, $desc){
		if(isset($this->formState['values']) && isset($this->formState['values'][$name])){
			$value = $this->formState['values'][$name];
		}else{
			$value = $desc['value'];
		}
		$output = '';
		$output .= '<input type="hidden" name="'. $name .'" value="'. $value .'"/>';
		return $output;
	}
}

?>