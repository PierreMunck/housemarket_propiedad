<?php 

class Hm_Profil extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'profil';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'id';
	
	/**
	 * campo de la tabla
	 */
	public $id = null;
	public $uid = null;
	public $data = null;
	public $expire = null;
	
	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'id' => 'int',
				'uid' => 'int',
				'data' => 'string',
				'expire' => 'date',
		);
	}

}
?>