<?php

class Hm_Pro_AtributoPropiedadList extends Hm_Db_TableList {

    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_ObjectClass = 'Hm_Pro_AtributoPropiedad';


    function __construct() {
    	parent::__construct();
    }

    public function getListPorIdioma($lang){
    	if(!isset($lang)){
    		return array();
    	}
    	$ListPorIdioma = array();
    	$lang = strtoupper($lang);
    	foreach($this->List as $attribute){
    		if($attribute->Idioma == $lang){
    			$ListPorIdioma[] = $attribute;
    		}
    	}
    	return $ListPorIdioma;
    }
}

?>