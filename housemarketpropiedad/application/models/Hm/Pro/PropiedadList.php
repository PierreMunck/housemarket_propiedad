<?php

class Hm_Pro_PropiedadList extends Hm_Db_TableList {

	protected $limit = 50;
	
	public $cluster = array();
	
	private $loadThumbnail = false;
	
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_ObjectClass = 'Hm_Pro_Propiedad';

    function __construct() {
    	parent::__construct();
    }
    
    public function lightload($Listrow = null){
    	if(!isset($Listrow)){
    		$Listrow = $this->List;
    		unset($this->List);
    		$this->List = array();
    	}
    	
    	foreach($Listrow as $row){
    		$propiedad = new stdClass();
    		if(!isset($row['Latitud'])){
    			continue;
    		}
    		if(!isset($row['Longitud'])){
    			continue;
    		}
    		if(!isset($row['PrecioVenta']) && !isset($row['PrecioAlquiler'])){
    			continue;
    		}
    		switch ($row['CodigoMoneda']){
    			case 1:
    				$row['MonedaCodidoEstandar'] = 'USD';
    				break;
    			case 2:
    				$row['MonedaCodidoEstandar'] = 'COP';
    				break;
    			case 3:
    				$row['MonedaCodidoEstandar'] = 'MXN';
    				break;
    		}
    		if(isset($row['PROFotoID'])){
    			$thumbnail = new Hm_Pro_Foto($row);
    			$propiedad->thumbPhoto = $thumbnail;
    		}
    		$propiedad->Latitud = $row['Latitud'];
    		$propiedad->Longitud = $row['Longitud'];
    		$propiedad->NombreCategoria = $row['NombreCategoria'];
    		$propiedad->AtributoCuarto = intval($row['cuarto']);
    		$propiedad->AtributoBano = intval($row['bano']);
    		$propiedad->AtributoParqueo = intval($row['parqueo']);
    		$propiedad->LabelArea = ($row['Area'] > 0) ? intval($row['Area']) . ' ' . $row['UnidadMedida'] : "-";
    		$propiedad->LabelAreaLote = ($row['AreaLote'] > 0) ? intval($row['AreaLote']) . ' ' . $row['UnidadMedidaLote'] : "-";;
    		$propiedad->CodigoPropiedad = $row['CodigoPropiedad'];
    		$propiedad->detallePrecioVentaSafe = $row['SimboloMoneda'] . $this->clearPrecio($row['PrecioVenta']) . ' <abbr title="<i18n>ISO_4217_CURRENCY_CODE_' . $row['MonedaCodidoEstandar'] . '</i18n>">'. $row['MonedaCodidoEstandar'] .'</abbr>';
    		$propiedad->detallePrecioAlquilerSafe = $row['SimboloMoneda'] . $this->clearPrecio($row['PrecioAlquiler']) . ' <abbr title="<i18n>ISO_4217_CURRENCY_CODE_' . $row['MonedaCodidoEstandar'] . '</i18n>">'. $row['MonedaCodidoEstandar'] .'</abbr>';
    		$this->List[] = $propiedad;
    	}
    }
    
    public function loationFiltro($coordinates){
    	if(isset($coordinates) && is_array($coordinates)){
	    	$this->filtroAgregado[] = " MBRContains(" .
	    		"GeomFromText('Polygon((" .
	    		$coordinates["minX"] . " " . $coordinates["minY"] . "," .
	    		$coordinates["minX"] . " " . $coordinates["maxY"] . "," .
	    		$coordinates["maxX"] . " " . $coordinates["maxY"] . "," .
	    		$coordinates["maxX"] . " " . $coordinates["minY"] . "," .
	    		$coordinates["minX"] . " " . $coordinates["minY"] .
	    		"))' ), " .
	    		"GeomFromWKB(Point(propropiedad.Longitud, propropiedad.Latitud))) ";
    	}
    }
    
    public function addCampoThumbnail(){
    	$this->loadThumbnail = true;
    }
    
    protected function modifySelect(&$select){
    	if($this->loadThumbnail){
    		$fields = array(
	    		'PROFotoID',
	    		'CodigoPropiedad',
	    		'Principal',
	    		'Comentario',
	    		'AltoFotoGrande',
	    		'AnchoFotoGrande',
	    		'Mime',
	    		'FechaCreacion',
	    		'FechaModifica',
	    	);
    		$select->joinLeft('profoto', 'propropiedad.CodigoPropiedad = profoto.CodigoPropiedad',$fields);
    		$where = 'profoto.Principal = 1';
    		$select->where($where);
    	}
    }
    
	private function clearPrecio($val){
    	$val = floatval($val);
    	return number_format($val);
    }
}

?>