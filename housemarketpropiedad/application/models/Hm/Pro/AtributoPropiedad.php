<?php

class Hm_Pro_AtributoPropiedad extends Hm_Db_Table {

    /**
     * Nombre de la tabla de propiedad con la cual se asocia
     * @var String
     */
    protected $_name = 'proatributopropiedad';
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_primary = 'CodigoPropiedad';
    //CodigoPropiedad
    //CodigoAtributo

    /**
     * campo de la tabla
     */

    public $CodigoPropiedad = null;
    public $CodigoAtributo = null;
    public $ValorEntero = null;
    public $ValorDecimal = null;
    public $ValorCaracter = null;
    public $ValorS_N = null;
    
    /**
     * joined campo
     */
    
    public $NombreAtributo = null;
    public $TipoAtributo = null;
    public $NombreEN = null;
    public $Idioma = null;
    public $LableAtributo = null;
    
    protected $_dependentTables = array(
    		'catatributo' => array(
    				'type' => 'strict',
    				'value' => 'proatributopropiedad.CodigoAtributo = catatributo.CodigoAtributo',
    		),
    		
    		'catatributoidioma' => array(
    				'type' => 'strict',
    				'value' => 'catatributo.CodigoAtributo = catatributoidioma.CodigoAtributo',
    		),
    );
    
    /**
     * descripcion de la Tabla
     * @return multitype:string
     */
    public function TableDescribe(){
    	return array(
    			'CodigoPropiedad' => 'int',
    			'CodigoAtributo' => 'int',
    			'ValorEntero' => 'int',
    			'ValorDecimal' => 'int',
    			'ValorCaracter' => 'string',
    			'ValorS_N' => 'string',
    			);
    }

    function __construct($proId = null, $attrId = null) {
    	if(is_int($proId) && is_int($attrId)){
    		$this->CodigoPropiedad = $proId;
    		$this->CodigoAtributo = $attrId;
    	}
    	parent::__construct();
    }

    public function populate($values) {
    	parent::populate($values);
    	
    	$this->NombreAtributo = $values['NombreAtributo'];
    	$this->TipoAtributo = $values['TipoAtributo'];
    	$this->NombreEN = $values['NombreEN'];
    	$this->Idioma = $values['CodigoIdioma'];
    	$this->LableAtributo = $values['ValorIdioma'];
    }
    
}

?>