<?php

class Hm_Pro_Favorito extends Hm_Db_Table {

    /**
     * Nombre de la tabla de propiedad con la cual se asocia
     * @var String
     */
    protected $_name = 'profavorito';
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_primary = 'CodigoFavorito';

    /**
     * campo de la tabla
     */

    public $CodigoFavorito = null;
    public $CodigoPropiedad = null;
    public $CodigoCliente = null;
    
    /**
     * descripcion de la Tabla
     * @return multitype:string
     */
    public function TableDescribe(){
    	return array(
    			'CodigoFavorito' => 'int',
    			'CodigoPropiedad' => 'int',
    			'CodigoCliente' => 'int',
    			);
    }

    function __construct($userId = null , $propiedadId = null) {
    	parent::__construct();
    	if(isset($userId)){
    		$this->CodigoCliente = $userId;
    	}
    	if(isset($propiedadId)){
    		$this->CodigoPropiedad = $propiedadId;
    	}
    }

    public function load($primaryKeyVal = null) {
    	if(!isset($primaryKeyVal) && !isset($this->CodigoFavorito)){
    		if(isset($this->CodigoPropiedad) && isset($this->CodigoCliente)){
    			parent::search(array('CodigoPropiedad' => $this->CodigoPropiedad,'CodigoCliente' => $this->CodigoCliente));
    		}
    	}else{
    		parent::load($primaryKeyVal);
    	}
    }
    
    public function delete() {
    	if($this->CodigoFavorito == null){
    		//delet con las otras informacion
    		if(isset($this->CodigoPropiedad) && isset($this->CodigoCliente)){
    			
    			$params = array('CodigoPropiedad' => $this->CodigoPropiedad, 'CodigoCliente' => $this->CodigoCliente);
    			$this->search($params);
    		}
    	}
    	parent::delete();
    }
    
    public function isfavorit() {
    	if($this->CodigoFavorito != null){
    		return true;
    	}
    	return false;
    }
}

?>