<?php 

class Hm_Pro_FotoAlbum extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'profotoalbum';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'aid';
	
	/**
	 * campo de la tabla
	 */
	public $aid = null;
	public $locacion = null;
	public $idpropiedad = null;
	public $size = null;
	public $creado = null;
	public $modificado = null;
	public $fbUser_id = null;
	public $img_default = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'aid' => 'int',
				'locacion' => 'string',
				'idpropiedad' => 'int',
				'size' => 'int',
				'creado' => 'date',
				'modificado' => 'date',
				'fbUser_id' => 'int',
				'img_default' => 'string',
		);
	}
	
}
?>