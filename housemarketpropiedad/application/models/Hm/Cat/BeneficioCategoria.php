beneficiocategoria

<?php 

class Hm_Cat_BeneficioCategoria extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catbeneficiocategoria';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = array('CodigoCategoria','CodigoBeneficio');
	
	/**
	 * campo de la tabla
	 */
	public $CodigoCategoria = null;
	public $CodigoBeneficio = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoCategoria' => 'int',
				'CodigoBeneficio' => 'int',
		);
	}
	
}

?>