<?php 
class Hm_Cat_BeneficioIdioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catbeneficioidioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CATBeneficioIdiomaID';
	
	/**
	 * campo de la tabla
	 */
	public $CATBeneficioIdiomaID = null;
	public $CodigoBeneficio = null;
	public $CodigoIdioma = null;
	public $ValorIdioma = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CATBeneficioIdiomaID' => 'int',
				'CodigoBeneficio' => 'int',
				'CodigoIdioma' => 'string',
				'ValorIdioma' => 'string',
		);
	}
	
}
?>