<?php 

class Hm_Cat_TipoEnlista extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'cattipoenlista';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoEnlista';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoEnlista = null;
	public $NombreEnlista = null;
	public $MostrarBusqueda = null;
	public $MostrarAgregar = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoEnlista' => 'string',
				'NombreEnlista' => 'string',
				'MostrarBusqueda' => 'string',
				'MostrarAgregar' => 'string',
		);
	}
	
}

?>