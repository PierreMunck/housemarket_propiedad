<?php 

class Hm_Cat_Pais extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catpais';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'ZipCode';
	
	/**
	 * campo de la tabla
	 */
	public $ZipCode = null;
	public $Pais = null;
	public $CodigoMoneda = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'ZipCode' => 'string',
				'Pais' => 'string',
				'CodigoMoneda' => 'int',
		);
	}
	
}
?>