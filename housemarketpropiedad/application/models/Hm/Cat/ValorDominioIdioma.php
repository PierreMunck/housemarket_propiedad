<?php 

class Hm_Cat_ValorDominioIdioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'CATValorDominioIdioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CATValorDominioIdiomaID';
	
	/**
	 * campo de la tabla
	 */
	public $CATValorDominioIdiomaID = null;
	public $CodigoValor = null;
	public $CodigoIdioma = null;
	public $ValorIdioma = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CATValorDominioIdiomaID' => 'int',
				'CodigoValor' => 'int',
				'CodigoIdioma' => 'string',
				'ValorIdioma' => 'string',
		);
	}
	
}

?>