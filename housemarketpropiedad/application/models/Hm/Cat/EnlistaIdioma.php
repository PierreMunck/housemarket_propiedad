<?php 

class Hm_Cat_EnlistaIdioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catenlistaidioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CATEnlistaIdiomaID';
	
	/**
	 * campo de la tabla
	 */
	public $CATEnlistaIdiomaID = null;
	public $CodigoEnlista = null;
	public $CodigoIdioma = null;
	public $ValorIdioma = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CATEnlistaIdiomaID' => 'int',
				'CodigoEnlista' => 'string',
				'CodigoIdioma' => 'string',
				'ValorIdioma' => 'string',
		);
	}
}
?>