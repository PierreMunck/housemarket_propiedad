<?php

class Hm_Cat_Dominio extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'CATDominio';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoDominio';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoDominio = null;
	public $NombreDominio = null;
	public $Estado = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoDominio' => 'int',
				'NombreDominio' => 'string',
				'Estado' => 'string',
		);
	}
	
}

?>