<?php 

class Hm_Cat_AtributoCategoria extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catatributocategoria';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = array('CodigoCategoria','CodigoAtributo');
	
	/**
	 * campo de la tabla
	 */
	public $CodigoCategoria = null;
	public $CodigoAtributo = null;
	public $OrdenPresenta = null;
	public $CampoDestino = null;
	public $Filtro = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoCategoria' => 'int',
				'CodigoAtributo' => 'int',
				'OrdenPresenta' => 'int',
				'CampoDestino' => 'string',
				'Filtro' => 'bool',
		);
	}
	
}
?>