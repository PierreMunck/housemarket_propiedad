<?php 

class Hm_SessionDb extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'sessiondb';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = array('session_id','save_path','name');
	
	/**
	 * campo de la tabla
	 */
	public $session_id = null;
	public $save_path = null;
	public $name = null;
	public $modified = null;
	public $lifetime = null;
	public $session_data = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'session_id' => 'string',
				'save_path' => 'string',
				'name' => 'string',
				'modified' => 'int',
				'lifetime' => 'int',
				'session_data' => 'string',
		);
	}

}
?>