<?php 
class Hm_Fb_Page extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'fb_page';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'id';
	
	/**
	 * campo de la tabla
	 */
	public $id = null;
	public $uid = null;
	public $pageid = null;
	public $dateIngreso = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'id' => 'int',
				'uid' => 'int',
				'pageid' => 'int',
				'dateIngreso' => 'date',
		);
	}
	
	public function save() {
		// verificacion de la llave unica pageid
		if(isset($this->uid) && isset($this->pageid)){
			if($exist = $this->search(array('pageid' => $this->pageid, 'uid' => $this->uid),true)){
				$this->id = $exist['id'];
			}
		}
		parent::save();
	}
}
?>