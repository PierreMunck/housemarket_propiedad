<?php 

class Hm_Cre_PromPage extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'prompage';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoReference';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoReference = null;
	public $Uid = null;
	public $PageReference = null;
	public $EmailReference = null;
	public $FechaRegistro = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoReference' => 'int',
				'Uid' => 'int',
				'PageReference' => 'int',
				'EmailReference' => 'string',
				'FechaRegistro' => 'date',
		);
	}
	
}
?>