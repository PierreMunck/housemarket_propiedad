<?php 

class Hm_Cre_CreditoPropiedad extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'crecreditopropiedad';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoCreditoProp';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoCreditoProp = null;
	public $CodigoPropiedad = null;
	public $CodigoCredito = null;
	public $FechaAsigna = null;
	public $FechaRetira = null;
	public $Cantidad = null;
	public $EstadoAsigna = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoCreditoProp' => 'int',
				'CodigoPropiedad' => 'int',
				'CodigoCredito' => 'int',
				'FechaAsigna' => 'date',
				'FechaRetira' => 'date',
				'Cantidad' => 'int',
				'EstadoAsigna' => 'string',
		);
	}
	
}
?>