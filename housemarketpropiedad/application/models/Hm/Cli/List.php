<?php 

class Hm_Cli_List extends Hm_Db_TableList {
	
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'cliente';
	
	function __construct() {
		$this->_setAdapter('db');
	}
	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function getClientePageinfo($pageIds){
		
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from('fb_page',array('uid','pageid'));
		$select->where('fb_page.pageid IN (?)', $pageIds);
		
		$select->join('cliente', 'fb_page.uid = cliente.Uid',array('NombreCliente','Email'));
		$select->join('page_service_status', 'fb_page.pageid = page_service_status.pageId',array('pageName'));
		$select->joinleft('page', 'fb_page.pageid = page.pageid',array('nombre','email'));
		
		$this->request = $select->assemble();

		$rs = $this->fetchAll($select);
		
		if(empty($rs)){
			return false;
		}
		
		if(empty($rs)){
			return false;
		}
		$result = $rs->toArray();
		$this->TotalResult = count($result);
		if(is_array($result)){
			$this->List = $result;
			return $result;
		}
		return false;
	}

}
?>