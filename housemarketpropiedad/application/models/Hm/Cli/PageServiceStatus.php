<?php 

class Hm_Cli_PageServiceStatus extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'page_service_status';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'id';
	
	/**
	 * campo de la tabla
	 */
	public $id = null;
	public $pageId = null;
	public $pageName = null;
	public $serviceType = null;
	public $serviceStatus = null;
	public $serviceStartDate = null;
	public $serviceExpirationDate = null;
	public $hmNotification = null;
	public $clientNotification = null;
	public $requestDate = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'id' => 'int',
				'pageId' => 'int',
				'pageName' => 'string',
				'serviceType' => 'int',
				'serviceStatus' => 'int',
				'serviceStartDate' => 'date',
				'serviceExpirationDate' => 'date',
				'hmNotification' => 'int',
				'clientNotification' => 'int',
				'requestDate' => 'date',
		);
	}
	
	public function save() {
		// verificacion de la llave unica pageId
		if(isset($this->pageId)){
			if($exist = $this->search(array('pageId' => $this->pageId),true)){
				$this->id = $exist['id'];
			}
		}
		parent::save();
	}
}
?>