<?php 
class Hm_Rmt_Beneficio extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'RMTBeneficio';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'RMTBeneficioID';
	
	/**
	 * campo de la tabla
	 */
	public $RMTBeneficioID = null;
	public $BeneficioCuarto = null;
	public $FechaCreacion = null;
	public $FechaModificacion = null;
	public $CodigoCuarto = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'RMTBeneficioID' => 'int',
				'BeneficioCuarto' => 'int',
				'FechaCreacion' => 'date',
				'FechaModificacion' => 'date',
				'CodigoCuarto' => 'int',
		);
	}
	
}
?>