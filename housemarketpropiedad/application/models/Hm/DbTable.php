<?php

class Hm_DbTable extends Zend_Db_Table {

	/**
	 *
	 * @var Zend_Adapter
	 */
	public $dbAdapter;
	
	/**
	 * Realiza una consulta con la base de datos
	 * @param String $sql Consulta a realizar
	 * @param array $params Parametros que se le pasan a la consulta a realizar
	 * @return RowSet Resultado de la consulta
	 */
	private function DoQuery ($sql, $params=null) {
		
		if (isset($params)) {
			$rs = $this->_db->query($sql,$params);
		} else {
			$rs = $this->_db->query($sql);
		}
		$data = $rs->fetchAll();
		return $data;
		
	}
}