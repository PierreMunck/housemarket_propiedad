<?php 

class Hm_GeoIp_GeoliteCityLocation extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'geolitecity_location';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'locID';
	
	/**
	 * campo de la tabla
	 */
	public $locID = null;
	public $country = null;
	public $region = null;
	public $city = null;
	public $postalCode = null;
	public $latitude = null;
	public $longitude = null;
	public $dmaCode = null;
	public $areaCode = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'locID' => 'int',
				'country' => 'string',
				'region' => 'string',
				'city' => 'string',
				'postalCode' => 'string',
				'latitude' => 'int',
				'longitude' => 'int',
				'dmaCode' => 'string',
				'areaCode' => 'string',
		);
	}
	
}
?>