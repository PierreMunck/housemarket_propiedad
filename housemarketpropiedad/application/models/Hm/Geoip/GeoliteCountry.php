<?php 

class Hm_GeoIp_GeoliteCountry extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'geolite_country';

	/**
	 * campo de la tabla
	 */
	public $start_ip = null;
	public $end_ip = null;
	public $start_num = null;
	public $end_num = null;
	public $cc = null;
	public $cn = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'start_ip' => 'string',
				'end_ip' => 'string',
				'start_num' => 'int',
				'end_num' => 'int',
				'cc' => 'string',
				'cn' => 'string',
		);
	}
}
?>