<?php

/**
 * Controller por defecto
 */
class PropiedadController extends Hm_MainController {

    /**
     *
     * @var <type>
     */
    protected $_flashMessenger = null;
    /**
     *
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializacion del controller
     */
    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        //$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
    	parent::preDispatch();
    }
    
    /**
     * Buscar en el mapa
     */
    public function searchAction() {
    	
    }

    /**
     * Buscar en el mapa
     */
    public function addAction() {
    	
    }
    
    public function likeAction() {
    	$propiedadId = $_GET['id'];
    	$favorito = new Hm_Pro_Favorito($this->Facebook_Signed->get('user_id'),$propiedadId);
    	$favorito->save();
    	// Redirigir al perfil
    	$redirect = '/show/propiedad/'. $propiedadId;
    	if(isset($_GET['ref'])){
    		$redirect = $_GET['ref'];
    	}
    	if(isset($_GET['signed_request'])){
    		$redirect .= '&signed_request='. $_GET['signed_request'];
    	}
    	$this->_redirect($redirect);
    }
    
    public function unlikeAction() {
    	$propiedadId = $_GET['id'];
    	$favorito = new Hm_Pro_Favorito($this->Facebook_Signed->get('user_id'),$propiedadId);
    	$favorito->delete();
    	// Redirigir al perfil
    	$redirect = '/show/propiedad/'. $propiedadId;
    	if(isset($_GET['ref'])){
    		$redirect = $_GET['ref'];
    	}
    	if(isset($_GET['signed_request'])){
    		$redirect .= '&signed_request='. $_GET['signed_request'];
    	}
    	$this->_redirect($redirect);
    }
}

?>