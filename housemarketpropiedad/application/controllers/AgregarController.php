<?php

/**
 * Controller por defecto
 */
class AgregarController extends Hm_MainController {

    /**
     *
     * @var <type>
     */
    protected $_flashMessenger = null;
    /**
     *
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializacion del controller
     */
    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        //$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
    	parent::preDispatch();
    }
    
    /**
     * Buscar en el mapa
     */
    public function propiedadAction() {
    	$this->appendStylesheet('/css/add_propiedad.css');
    	$this->loadJQueryPlugin('gmap');
    	$this->appendScriptFile('/js/gmap-add.js');
    }

}

?>