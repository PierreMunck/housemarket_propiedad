<?php

/**
 * Controller por defecto
 */
class ShowController extends Hm_MainController {

    /**
     *
     * @var <type>
     */
    protected $_flashMessenger = null;
    /**
     *
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializacion del controller
     */
    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        //$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
    	parent::preDispatch();
    }
    
    /**
     * Buscar en el mapa
     */
    public function propiedadAction() {
    	$signed = Zend_Registry::get('Facebook_Signed');
    	
    	$id_url_propiedad = $this->uri[3];
    	
    	if(is_numeric($id_url_propiedad)){
    		$propId = (int) $id_url_propiedad;
    		$propiedad = new Hm_Pro_Propiedad($propId);
    		$propiedad->load();
    	}
    	if(is_String($id_url_propiedad)){
    		//TODO sistem por clear url
    	}
    	if(isset($propiedad) && $propiedad->validate()){
    		if(isset($propiedad->Uid)){
    			//recuperamos las informaciones del vendedor
    			$broker = new Hm_Cli_Cliente();
    			$broker->search(array('Uid' => $propiedad->Uid));
    			$broker->PicBig = 'https://graph.facebook.com/'. $broker->Uid .'/picture?type=large';
    			$broker->PicSmall = 'https://graph.facebook.com/'. $broker->Uid .'/picture?type=normal';
    			$broker->Url = 'https://www.facebook.com/'. $broker->Uid;
    			// verificamos que el vendedor no hace parte de un grupo
    			$pagelist =  new Hm_Fb_PageList();
    			$pagelist->search(array('uid' => $propiedad->Uid));
    			foreach($pagelist as $pagebroker){
    				$pagebrokerInfo = null;
    				try {
    					$app = Zend_Registry::get('Facebook_App');
    					$pagebrokerInfo = $app->api('/'.$pagebroker->pageid);
    					if(isset($pagebrokerInfo)){
    						$broker->PicBig = 'https://graph.facebook.com/'. $pagebroker->pageid .'/picture?type=large';
    						$broker->PicSmall = 'https://graph.facebook.com/'. $pagebroker->pageid .'/picture?type=normal';
    						$broker->NombreCliente = $pagebrokerInfo['name'];
    						$broker->Url = 'https://www.facebook.com/'. $pagebroker->pageid;
    						break;
    					}
    				}catch (Exception $e){
    				}
    			}
    		}
    		
    		// set facebook og
    		
    		$this->view->headerMeta->setProperty('og:type','housemarkettab:propiedad');
    		$this->view->headerMeta->setProperty('og:url', $this->cfg['local_app_url'].'/show/propiedad/'.$propiedad->CodigoPropiedad);
    		$desc = $propiedad->NombreCategoria .' - '. $propiedad->detallePrecioVentaSafe .' - '. $propiedad->detallePrecioAlquilerSafe .' , '. $propiedad->LabelLocation;
    		$this->view->headerMeta->setProperty('og:title',$desc);
    		$this->view->headerMeta->setProperty('og:description',$desc);
    		$this->view->headerMeta->setName('title',$desc);
    		$this->view->headerMeta->setName('description',$propiedad->DescPropiedad);
    		//$this->view->headerMeta->setlink('target_url',$this->cfg['local_app_url'].'/show/propiedad/'.$propiedad->CodigoPropiedad);
    		
    		// form contact
    		$formState = array();
    		$formState['PropiedadId'] =  $propiedad->CodigoPropiedad;
    		$formState['BrokerId'] = $broker->Uid;
    		$formContact = new Hm_Form_Contact($signed,$formState);
    		$formContact->run();
    		$this->view->formContact = $formContact;
    		
    		// css
    		$this->view->headLink()->appendStylesheet('/css/propiedad.css');
    		$this->loadJQueryPlugin('infinitecarousel');
    		$this->loadJQueryPlugin('opacityrollover');
    		$this->loadJQueryPlugin('gmap');
    		
    		$this->view->headScript()->appendFile('/js/sharebutton.js');
    		$this->view->headerScript->setScript(
				'	!function(d,s,id){
						var js,fjs=d.getElementsByTagName(s)[0];
						if(!d.getElementById(id)){
							js=d.createElement(s);
							js.id=id;js.src="//platform.twitter.com/widgets.js";
		                	fjs.parentNode.insertBefore(js,fjs);
		                }
		            }
		            (document,"script","twitter-wjs");'
            );
    		$this->view->headerScript->setScript(
    		'$(function(){
    			$(\'#carousel\').infiniteCarousel({
    				displayTime: 12000,
    				textholderHeight : .25,
					displayProgressBar : 0,
					displayThumbnails: 0,
					displayImgPos: 1,
    				displaydesc: 0
    			});
    		});'
    		);
    		
    		// load foto
    		$fotos = new Hm_Pro_FotoList();
    		$fotos->search(array('CodigoPropiedad' => $propiedad->CodigoPropiedad));
    		$propiedad->fotos = $fotos;
    		$propiedad->thumbPhoto = $fotos->get(0);
    		if(is_object($propiedad->thumbPhoto)){
    			$this->view->headerMeta->setProperty('og:image',$propiedad->thumbPhoto->getUrl('Principal'));
    			$this->view->headerMeta->setLink('image_src',$propiedad->thumbPhoto->getUrl('Principal'));
    		}
    		$attributos = array();
    		if(isset($propiedad->ListAttributo)){
    			$attributos = $propiedad->ListAttributo->getListPorIdioma($this->language);
    		}
			
    		// Atributos de la propiedad
    		$this->view->AtributoPropiedad = array();
    		foreach($attributos as $attributo){
    			$attributo->ValorBool = '';
    			if (strtoupper($attributo->ValorS_N) == 'S'){
    				$attributo->ValorBool = "<i18n>LABEL_YES</i18n>";
    			}
    			else if (strtoupper($attributo->ValorS_N) == 'N'){
    				$attributo->ValorBool = "<i18n>LABEL_NO</i18n>";
    			}
    			
    			$att = "br";
    			if ($attributo->CodigoAtributo < 3) {
    				if ($attributo->CodigoAtributo == 2) {
    					$att = "ba";
    				}
    				$float_redondeado = round($attributo->ValorDecimal);
    				$testprop = '';
    				if ($float_redondeado == 0) {
    					$testprop.= $attributo->ValorEntero . " " . $att . " | ";
    				} else {
    					$testprop.= $float_redondeado . " " . $att . " | ";
    				}
    			}
    			
    			$labelAppend = '';
    			if ($attributo->NombreAtributo === 'HOA') {
    				$labelAppend = ' <abbr title="<i18n>ISO_4217_CURRENCY_CODE_' . $propiedad->MonedaCodidoEstandar . '</i18n>">'
    				. $propiedad->MonedaCodidoEstandar . '</abbr>';
    			}

    			$values = array();
    			$values['Label'] = $attributo->NombreAtributo . $labelAppend;
    			$values['Value'] = $attributo->ValorEntero . $attributo->ValorDecimal . $attributo->ValorBool . $attributo->ValorCaracter;

    			$this->view->AtributoPropiedad[] = $values;
    		}
    		
    		// print precio de venta o de alquila segun el modo elegido
    		if (!empty($propiedad->detallePrecioVenta)){
    			$propiedad->detallePrecio = $propiedad->detallePrecioVenta;
    			$propiedad->detallePrecioSafe = $propiedad->detallePrecioVentaSafe;
    		}
    		
    		if (!empty($propiedad->detallePrecioAlquiler)){
    			$propiedad->detallePrecio = $propiedad->detallePrecioAlquiler;
    			$propiedad->detallePrecioSafe = $propiedad->detallePrecioAlquilerSafe;
    		}
    		
    		$this->view->favorito = false;
    		$favorito = new Hm_Pro_Favorito();
    		$param = array(
    				'CodigoPropiedad' => $propiedad->CodigoPropiedad,
    				'CodigoCliente' => $this->Facebook_Signed->get('user_id')
    				);
    		$favorito->search($param);
    		
    		if($favorito->CodigoFavorito != null){
    			$this->view->favorito = true;
    		}
    		
    		$this->view->propiedad = $propiedad;
    		$this->view->broker = $broker;
    		$this->view->headerScript->setScript(
    				'$().ready(function(){
    						$("#mapsearch").gMap({
				controls: {
			         panControl: false,
			         zoomControl: true,
			         mapTypeControl: false,
			         scaleControl: false,
			         streetViewControl: false,
			         overviewMapControl: false
			     },
    			markers: [{
    				latitude: '. $propiedad->Latitud .',
    				longitude: '. $propiedad->Longitud .',
    				icon: { image: "/img/gmap_pin.png",
                                      iconsize: [29, 29],
                                      iconanchor: [12,29],
                                      infowindowanchor: [12, 0] } 
    			}],
    			zoom: 15 
    			});
    	});'
    		);
    	}else{
    		$this->render('nopropiedad');
    	}
    }

}

?>