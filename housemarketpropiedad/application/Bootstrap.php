<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	protected $_logger;
	public $dbAdapter;
	
	protected function _initConfig() {
		Zend_Registry::set('config', $this->getOptions());
	}
	
	protected function _initLogging() {
		$logger = new Zend_Log();
	
		$writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/app.log');
		$logger->addWriter($writer);
	
		/*if ('production' == $this->getEnvironment()) {
			$filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
			$logger->addFilter($filter);
		}*/
		
		$this->_logger = $logger;
		Zend_Registry::set('log', $this->_logger);
	}
	
	protected function _initDB() {
		$resource = $this->getPluginResource('db');
		try {
			// iniciado con las resources en configuracion (configs/application.ini)
			$this->dbAdapter = $resource->getDbAdapter();
			// usando el sistema objecto de base de datos
			$this->dbAdapter->setFetchMode(Zend_Db::FETCH_OBJ);
			// el adaptador esta en registry
			Zend_Registry::set('db', $this->dbAdapter);
		} catch (Zend_Db_Adapter_Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
			die("Fallo de conexion, revisar host/username/password");
		} catch (Zend_Exception $e) {
			die('Fallo en la carga de la clase Adapter');
		}
		
		// Message System
		include_once('Hm/System/Message.php');
		$message = new Hm_System_Message();
		Zend_Registry::set('message', $message);
		
		return;
	}
	
	protected function _initLanguage() {
		
		// del usuario
		$info_locale="es";
		if (Zend_Registry::isRegistered('signed')) {
			$info = Zend_Registry::get('signed');
			if(isset($info['user']) && isset($info['user']['locale'])){
				$info_locale = $info['user']['locale'];
			}
		}
		
		// filtro de las lenguas conocidas
		$language = substr($info_locale, 0, 2);
		$appLanguages = array("en", "es");
		if (!in_array($language, $appLanguages)) {
			$language = "es";
		}
	
		// generacion de la Locale
		$locale = new Zend_Locale($language);
		Zend_Registry::set('Zend_Locale', $locale);
		Zend_Registry::set('language', $language);
	
		// inicialisacion del sistema de translacion
		$translationFile = APPLICATION_PATH .'/lang/'. $language . '.inc.php';
		$translate = new Zend_Translate('array', $translationFile, $language);
		Zend_Registry::set('Zend_Translate', $translate);
	}
	
	protected function _initUser() {
		$signed = null;
		$signed_request = null;
		
		$cfg = $this->getOptions();
		// iniciando la applicacion Facebook
		try{
			$appfb = new Facebook_Facebook(array('appId' => $cfg['facebook_appid'], 'secret' => $cfg['facebook_secret']));
			Zend_Registry::set('Facebook_App', $appfb);
		} catch (FacebookApiException $e) {
			$this->$_logger->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
			die();
		}
		$signed = $appfb->getSignedRequest();
		// get from facebook API
		if(isset($appfb) && !isset($signed['user_id'])){
			if(isset($_GET['code'])){
				$appfb->safecheck($_GET['code']);
				$signed = $appfb->getSignedRequest();
			}
			error_log('SignedRequest user 0');
		}
		$signed_request = new Hm_Facebook_SignedRequest($signed);
		Zend_Registry::set('Facebook_Signed', $signed_request);
	}
	
	protected function _initViewSettings() {
	
		$this->bootstrap('view');

	
		$this->_view = $this->getResource('view');
		$this->view->addFilterPath('Hm/View/Filter', 'Hm_View_Filter');
		$this->view->setFilter('Translate');
	
		// add global helpers
		/*
		$this->_view->addHelperPath(APPLICATION_PATH . '/views/helpers', 'Zend_View_Helper');
		$this->_view->addHelperPath(APPLICATION_PATH . '/views/helpers', 'Dgt');
		*/	
	
		// set encoding and doctype
		$this->_view->setEncoding('utf-8');
	
		//$this->_view->doctype('XHTML1_RDFA'); seria bueno pero no funciona en esa version de Zend
		$this->_view->doctype('XHTML1_TRANSITIONAL');
	
		
		// set the content type and language
		$locale = Zend_Registry::get('Zend_Locale');
		$this->_view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8'); //iso-8859-1
		$this->_view->headMeta()->appendHttpEquiv('Content-Language', 'es-NI');
	
		// set css links and a special import for the accessibility styles
		/*
		$this->_view->headLink()->appendStylesheet('/css/facebook-styles.css');
		$this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
		$this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
		$this->_view->headScript()->appendFile("/js/facebook.js?v=2");
		*/
		
		// setting the site in the title
		$this->_view->headTitle('Housemarket');
	
		// setting a separator string for segments:
		$this->_view->headTitle()->setSeparator(' - ');
	
		/*$d = 'Housebook es una aplicación sencilla y amigable que te ayuda a intercambiar información de bienes races, conectate con las personas que buscan comprar, vender, rentar y/o alquilar propiedades alrededor del mundo.';
		$this->_view->descripcion = $d;
		if(isset($_GET['noframe'])&&$_GET['noframe']==true){
			$this->bootstrap('layout');
			$layout=$this->getResource('layout');
			$layout->setLayout('noframe');
			$layout->enableLayout();
		}*/
	}
}